#+TITLE:     Crowdfunding Free Software
#+AUTHOR:    Deb Nicholson & Christopher Allan Webber
#+EMAIL:     press@mediagoblin.org
#+DATE:      2014-10-24 Fri

#+OPTIONS: reveal_center:t reveal_progress:t reveal_history:nil reveal_control:t
#+OPTIONS: reveal_mathjax:t reveal_rolling_links:t reveal_keyboard:t reveal_overview:t num:nil
#+OPTIONS: reveal_width:1200 reveal_height:800
#+OPTIONS: toc:nil
#+REVEAL_MARGIN: 0.075
#+REVEAL_MIN_SCALE: 0.5
#+REVEAL_MAX_SCALE: 2.5
#+REVEAL_TRANS: cube
#+REVEAL_THEME: night
#+REVEAL_HLEVEL: 2
#+REVEAL_HEAD_PREAMBLE: <meta name="description" content="Crowdfunding Free Software">

* Introductions
    :PROPERTIES:
    :reveal_background: ../static/CC.BY_JeremySchultz_Gold-dull.jpg
    :END:

** About Deb

# Chris introduces Deb

#+REVEAL_HTML: <img style="float: right;" src="../static/deb_photo-crop.jpg" />

# TODO: Picture of Deb here?
# Probably the one from the ada initiative one...

# #+ATTR_REVEAL: :frag roll-in
 - Long-time political activist!
 - Free software activist!
   - MediaGoblin co-founder / community
   - Now: OIN, OpenHatch, other stuff
   - Formerly: Ada Initiative, FSF
 - Co-organizer of MediaGoblin campaign!

** About Chris

# Deb introduces Chris

#+REVEAL_HTML: <img style="float: right;" src="../../network_freedom/static/libreplanet_toon_chris.png" />

# #+ATTR_REVEAL: :frag roll-in
 - Lead MediaGoblin developer!
 - Free software and free culture activist
   - MediaGoblin co-founder / lead dev
   - Now: full time MediaGoblin
   - Formerly: CC, PCF, other things
 - Liberated Pixel Cup!
 - MediaGoblin crowdfunding campaign!

** MediaGoblin

#+REVEAL_HTML: <img style="height: 550px;" src="../../mediagoblin/static/mediagoblin_mascot.png" />

MediaGoblin is a free software media publishing system anyone can use
and run!

** MediaGoblin's campaigns

#+REVEAL_HTML: <img style="height: 550px;" src="../../network_freedom/static/2014_campaign.png" />

We ran two fundraising campaigns with the FSF!

Between the two, over 100k raised

*** Campaign page and video

Let's see the campaign page: http://mediagoblin.org/pages/campaign.html

Going to show the video (clear intro for the project, and useful for
later...)

* Making your plan
    :PROPERTIES:
    :reveal_background: ../static/CC.BY_JeremySchultz_Gold-dull.jpg
    :END:

** So you want to run a crowdfunding campaign

#+REVEAL_HTML: <img style="height: 600px;" src="../static/CC.BY.SA_KaiScheiber_endOfTheRainbow-scaled.jpg" /><br />

** The "crowdfunding phenomena"

#+REVEAL_HTML: <img style="height: 600px;" src="../static/potato_salad_kickstarter.png" />

** Quick overview: Introducing the timeline 

Launching a campaign takes about 3 months...

 - General planning:
   - What are you doing, why?
   - Who's your audience?
   - Where are you running it?
   - Rewards?
 - Video (A whole thing in itself)
   - Planning
   - Executing
 - Prepping launch page
 - Gathering allies / news
 - Launch
 - Daily grind: keep it going
 - Celebrate!

*** And then

 - Producing, shipping rewards
 - Execution!
 - Regular updates to backers!

** Will it be ok if you miss your goal? 

#+REVEAL_HTML: <img style="height: 600px;" src="../static/comparing_old_news.png" />

MediaGoblin campaign 1 under MediaGoblin campaign 2 banner

*** What if it doesn't go viral a la Diaspora

#+REVEAL_HTML: <img style="height: 400px;" src="../static/CC.BY_DawnHuczek_SquirrelOutside-scaled.jpg" />

# #+ATTR_REVEAL: :frag roll-in
 - Most projects don't!
 - Use an "all or nothing" model?
   - Can you really afford it if you don't make it?
   - Are you going to do it anyway?

*** What if it does? (v1)

#+REVEAL_HTML: <img style="height: 400px;" src="../static/CC.BY.SA_likeaduck_SquirrelJackpot-scaled.jpg" /> <br />

# #+ATTR_REVEAL: :frag roll-in
 - Yay!  Jackpot!
 - But now the real work starts
 - Can you handle the overflow?

*** What if it does? (v2)

#+REVEAL_HTML: <img style="height: 600px;" src="../static/fate_core_success.png" />

*** What if it does? (v3)

#+REVEAL_HTML: <img style="height: 600px;" src="../static/book_burning_for_sad_children.png" />

*** Setting expectations internally, and externally

#+REVEAL_HTML: <img style="height: 300px; border: none;" src="../../network_freedom/static/unlock_characters.png" />

# #+ATTR_REVEAL: :frag roll-in


* Identifying your audience
    :PROPERTIES:
    :reveal_background: ../static/CC.BY_JeremySchultz_Gold-dull.jpg
    :END:
** Where to host it?

Use a crowdfunding platform, nonprofit
infrastructure, or do it yourself?

# #+ATTR_REVEAL: :frag roll-in
 - Doing it yourself
   - Lots of control
   - But lots of work
 - Using a hosted, proprietary solution
   - Convenient
   - Built in social networking tools
   - At the whims of said org
   - Fee going to a corporation
 - Work with a nonprofit
   - Use the same fundraising infrastructure the nonprofit does
   - Fiscal sponsorship: huge!
   - Tax deductable donations
   - Fee going to a nonprofit you presumably believe in

*** Free software solutions that are available?

#+REVEAL_HTML: <img style="height: 300px;" src="../static/civicrm.png" />

# #+ATTR_REVEAL: :frag roll-in
 - Increasing numbers of these nowadays...
   - geoteo.org
   - "bounty hunting" kind of crowdfunding (freedomsponsors, lots of older ones)
 - Roll your own!
   - Pitivi did this
   - Joey Hess did git-annex + ikiwiki + ledger
 - CiviCRM or other "traditional" fundraising tools
 - MediaGoblin used the FSF's CiviCRM + our own static site

** What are the major upsides?

#+REVEAL_HTML: <img style="height: 400px;" src="../static/cc.by.sa_Nick_Ares_Money.jpg" />

# #+ATTR_REVEAL: :frag roll-in
 - Fund your project without compromising your mission
 - Community is not just giving you money, but investing themselves
   personally
 - Learn how your project is perceived externally

** Major downsides?

#+REVEAL_HTML: <img style="height: 450px;" src="../static/metropolis.jpg" /> <br />

# #+ATTR_REVEAL: :frag roll-in
 - Timing, both external and internal
 - Running a campaign is another job
 - Fulfilling a campaign may be another job

* Running a campaign (or: no such thing as too much planning)
    :PROPERTIES:
    :reveal_background: ../static/CC.BY_JeremySchultz_Gold-dull.jpg
    :END:

** Plan your video early!

[[file:../../mediagoblin/static/censor_scan.png]]

Videos take a lot of work, and are one of the strongest sources of
impressions of a campaign these days...

How much work?  Depends on complexity; we followed standard-ish
film/animation steps:

*** Make script outline

file:../static/video_outline.png

*** Draft / refine script

file:../static/video_script.png

*** Make storyboard

file:../static/video_storyboard.png

*** Do animation tests

file:../static/video_animation_test.png

*** Do voiceover

file:../static/audio_editing.png

*** Shoot video

file:../static/video_chris_speaking.png

*** Animate

[[file:../../mediagoblin/static/campaign_scary_internet_blender.png]]

*** Video edit / sequencing

[[file:../../mediagoblin/static/campaign_seq_edit.png]]


** Planning rewards

#+REVEAL_HTML: <img style="height: 450px;" src="../static/all_rewards.png" />

Plan and price your rewards in advance of offering them
 - Should be no more than 25% of the reward "cost"
 - Shipping costs can kick your butt, esp internationally
 - Authoring times? Printing times?  Shipping times?

** Get your allies together

#+REVEAL_HTML: <img style="height: 450px;" src="../static/in_your_internet_decentralizing_your_medias.png" /> <br />

# TODO: Pic of friends

Make it easy for people to help you

** Consider "in-kind" help

[[file:../static/pycon_gavroche.png]]

You're most likely to get help if it works mutually

Cookie factory: more likely to give you cookies than money!

** Prep the presses

#+REVEAL_HTML: <img style="height: 350px;" src="../static/linux_magazine_article.png" />

 - Try to get press ready for your own launch, eg
   - Your announcement post (critical!)
   - Journalism
   - Allies
 - Have some of your own news planned ahead of time
   - Plan some things to announce mid-campaign
   - Have a list of people to reach out to throughout the campaign

** Campaign launch

#+REVEAL_HTML: <img style="height: 450px;" src="../static/goblin_force-postcard-cropped-scaled.png" /><br />

 - Launch is critical
 - Limited pre-launch?
 - Prep as best as you can, but don't let hiccups get you down

** Keeping it going

file:../static/mediagoblin_campaign_graph.png

** Common pitfalls

file:../static/rewards_estimates.png

 - Delivering rewards: don't lose your shirt sending shirts
 - Do or don't quit your day job?

** The end! (??)

#+REVEAL_HTML: <img style="height: 500px;" src="../static/CC.BY_AndyHay_PotofGoldUnderABench-scaled.jpg" /><br />

Congrats, you made it!

(Of course: now you need to ship rewards, and keep your promises!)

* Is it worth it?  Real financial and time breakdowns
    :PROPERTIES:
    :reveal_background: ../static/CC.BY_JeremySchultz_Gold-dull.jpg
    :END:

** Finances

#+REVEAL_HTML: <img style="height: 500px;" src="../static/mediagoblin_expense_breakdown.png" /> <br />

We did a complete financial breakdown:

http://mediagoblin.org/news/financial-transparency.html

** How much time does it take?

 - The first campaign took about 2.5 months to launch, ran for 1
 - Second campaign took about 4 months to launch, ran for 2
 - For me: 10-12 hour days, few weekends
 - Both have several weeks of work afterwards on rewards fulfillment
 - So really, about 3.5-7 months on the campaign on each
 - Remember: this is an unpaid period of work!

** So was it worth it? (post-mortem!)

# Do your post-mortem here

[[file:../static/ascii_yeowch_gavroche.png]]

 - It's not "easy money"... expect long periods of stress, weight
   gain, hair loss
 - But yes, it was worth it!
   - 2 person-years of full time development (and more)
   - We're accomplishing what we set out to do
   - We're staying true to our goals/values
   - Donors seem happy

** Credits

 - "End of the Rainbow" image by KaiScheiber, CC BY-SA 2.0
 - Squirrel jackpot image by likeaduck, CC BY-SA 2.0
 - Squirrel out in the cold by DawnHuczek, CC BY 2.0
 - Pot of gold and a bench by AndyHay, CC BY 2.0
 - Pile of gold coins background image, CC BY 2.0
 - Pile of cash, Nick Ares, CC BY 2.0

*** Credits (2)

 - Dude fighting a clock from Metropolis, fair use
 - Linux Magazine screen grab from
   http://www.linux-magazine.com/Online/Features/MediaGoblin-Saving-the-Internet-Through-Federation
 - Photo of Deb: Lindsay Metivier

Otherwise else, this talk is dual licensed:

CC BY-SA 4.0 International || GPLv3 or later

** Thank you! (Questions?)

#+REVEAL_HTML: <img style="height: 450px;" src="../static/goblin_force_badges.png" /> <br />

http://mediagoblin.org/

 - *Deb:* deb@eximiousproductions.org
 - *Chris:* cwebber@dustycloud.org
 - ~git clone https://gitlab.com/dustyweb/talks.git~
