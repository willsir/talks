Christopher Allan Webber's conference talks.

Triple licensed under CC BY-SA 3.0/4.0, GPLv3 or later.  Your choice!

To "compile" the talks, simply have make and emacs already
installed and run "make".

There are some older talks that are not run automatically, run "make
beaker-talks" to generate those.  Note, you'll also need LaTeX, LaTeX
beamer, and if you want syntax highlighting,
[Minted](https://code.google.com/p/minted/).
(Unfortunately, some of these need to be updated since OrgMode's
Beamer setup has changed since I wrote those talks...)

I recommend that you open the HTML "reveal" presentations in chromium.
I've found that renders better than Firefox.