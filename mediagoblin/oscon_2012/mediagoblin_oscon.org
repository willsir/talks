#+TITLE:     GNU MediaGoblin at OSCON
#+AUTHOR:    Christopher Allan Webber
#+EMAIL:     cwebber@creativecommons.org
#+DATE:      2012-07-20 Fri
#+DESCRIPTION:
#+KEYWORDS:
#+LANGUAGE:  en
#+OPTIONS:   H:3 num:t toc:nil \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+LINK_UP:
#+LINK_HOME:
#+XSLT:
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [bigger]
#+beamer_header_extra: \usecolortheme[RGB={77,15,92}]{structure} \usetheme{Madrid} \setbeamertemplate{navigation symbols}{} 
#+BEAMER_FRAME_LEVEL: 2
#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)

#+LATEX_HEADER: \hypersetup{colorlinks=true, urlcolor=cyan}

* Intro
** Hi!  I'm Chris Webber

 - Currently senior software engineer at Creative Commons... but
   transitioning out!
 - Also lead developer and project founder of GNU MediaGoblin
 - Haven't figured out how to make that sustainable but taking the
   dive anyways :)

** Logo
   :PROPERTIES:
   :BEAMER_envargs: [shrink = 11]
   :END:

:                               .-''-.
:                              < o  o >
:                              /oO-Ooo'\
:                         ///\/ -____-  \
:                          -->\.    ///\.\
:                          \__/\    -\_/ /
:                               )     \_/
:                              / \__/   ' 
:                             /  /  |  |
:                          ____ _   _ _   _ 
:                         / ___| \ | | | | |
:                        | |  _|  \| | | | |
:                        | |_| | |\  | |_| |
:                         \____|_| \_|\___/ 
:        __  __          _ _        ____       _     _ _       
:       |  \/  | ___  __| (_) __ _ / ___| ___ | |__ | (_)_ __  
:       | |\/| |/ _ \/ _` | |/ _` | |  _ / _ \| '_ \| | | '_ \ 
:       | |  | |  __/ (_| | | (_| | |_| | (_) | |_) | | | | | |
:       |_|  |_|\___|\__,_|_|\__,_|\____|\___/|_.__/|_|_|_| |_|

** Real logo

#+begin_latex
\begin{center}\includegraphics[height=0.82\textheight]{../static/mediagoblin_mascot_logo.png}\end{center}
#+end_latex

** What is MediaGoblin?

 - Media publication system
 - Think: decentralized Flickr/YouTube
 - Supports images, video, ascii art, and audio!
 - Extensible!  You can add new media types!  (3d?  Presenations?  Why
   not!)
 - Forward-facing!  HTML5 technologies!
 - I founded the project a little over a year ago
 - Huge contributor community!

** Screenshot!
   :PROPERTIES:
   :BEAMER_envargs: [shrink = 13]
   :END:

:                     | GNU MediaGoblin             | login | register |
:                     |------------------------------------------------|
:                     |                                                |
:                     |   ___________________________________________  |
:                     |  |                                           | |
:                     |  | GNU MediaGoblin is a great way to start   | |
:                     |  | sharing blah blah blah                    | |
:                     |  |                          [ Sign me up! ]  | |
:                     |  |                       [ More about GMG ]  | |
:                     |  '-------------------------------------------' |
:                     |                                                |
:                     |   ___________________________________________  |
:                     |  |_Featured_works____________________________| |
:                     |  |  _________      _________      _________  | |
:                     |  | | ,-\/    |    |  ,( )   |    |  ,,,    | | |
:                     |  | | /o  o_,,|    |  (_o_)  |    | e~_~    | | |
:                     |  | |/ __vvvv |    |    /    |    |  \ /    | | |
:                     |  | | /  \    |    | ,,,,,,  |    | /  \    | | |
:                     |  | '---------'    '---------'    '---------' | |
:                     |  |  Dragons of    Clover in       Joe is so  | |
:                     |  |  Flame by      the grass         Hip      | |
:                     |  |  schendje      by clovdud       by joe    | |
:                     |  '-------------------------------------------' |

** Another screenshot!
   :PROPERTIES:
   :BEAMER_envargs: [shrink = 13]
   :END:

:                               | GNU MediaGoblin             | login | register |
:                               |------------------------------------------------|
:                               |                                                |
:                               |  The Terror of Tinman Tim                      |
:                               |  Posted by cwebber on May 8, 2011              |
:                               |   ___________________________________________  |
:                               |  |                                           | |
:                               |  |      __ ____                              | |
:                               |  |  (O)== |    |=(O)                         | |
:                               |  |     |  |O  o|         DESTROY ALL HUMANS  | |
:                               |  |     |__|[ww]|                             | |
:                               |  |    |    |  _ |  _                         | |
:                               |  |    | ((((((_ |((_                         | |
:                               |  |    |    |    |                            | |
:                               |  |    |____|____|                            | |
:                               |  |      |_|  \_\  _                          | |
:                               |  |    _/_/_   \_// /                         | |
:                               |  |    |___||   |/_/                          | |
:                               |  |                                           | |
:                               |  '-------------------------------------------' |
:                               |    _________________________________________   |
:                               |   | Just another piece on a robot who wants |  |
:                               |   | to do what any robot would want to do.  |  |
:                               |   '-----------------------------------------'  |
:                               |                                                |
:                               |  Comments:                                     |
:                               |   __________________________________________   |
:                               |  | Wow I love your robot!!!!!!              |  |
:                               |  |                 sue @ May 8 2011 11:45PM |  |
:                               |  '------------------------------------------'  |

** Real screenshot :)
   :PROPERTIES:
   :BEAMER_envargs: [shrink = 15]
   :END:

#+begin_latex
\begin{center}\includegraphics[height=0.9\textheight]{../static/mediagoblin_screenshot.png}\end{center}
#+end_latex

** MediaGoblin intro part 2!

 - About 35-40 contributors to the project (~30 committers, including
   people from FSF & SFLC)
 - About 5-10 people actively contributing at any time
 - What I have spent pretty much all my vacation, weekends, holiday
   time on for the last year
 - A GNU project!  (Valuable as it's a principled foundation actually
   the organizational stewards of the project)

Probably best to click through http://mediagoblin.org/

* Why does this matter?
** The web is moving more and more in this direction

#+begin_latex
\begin{center}\includegraphics[height=0.82\textheight]{../static/centralized.png}\end{center}
#+end_latex

** But it's supposed to work like this

#+begin_latex
\begin{center}\includegraphics[height=0.82\textheight]{../static/federated.png}\end{center}
#+end_latex

** Sad internets

 - The internet and the web are *supposed* to be decentralized
 - What would happen if Flickr or YouTube went away today?
 - What about censorship?
 - What about defining your own communities?
 - Fortunately, we can make the internet un-sad again

** There's a better option!

 - Federation!
 - ... Wait, what's that?
 - Think: email, XMPP/Jabber, etc
 - We'll talk more about OStatus later...

** A bit on licensing choice

#+begin_latex
\begin{center}
#+end_latex
[[file:../static/agplv3.png]]
#+begin_latex
\end{center}
#+end_latex

** A few more words on copyleft (the objections)

There are some reasons possibly to object to copyleft
 - (free|open)bsd-esque copyleft is nonfree (because it uses
   copyright restrictions to regulate the commons)
   - this used to be a popular objection
   - really hard to maintain though because you have to object to all
     properietary software but object to methods to protect against it
     too... on the decline I think
 - copyleft is "strategically suboptimal"... we can get more
   collaboration by working with people who are wary of copyleft or
   who give away everything but their "secret sauce"
   - legitimate point, I think!
   - when we're talking about strategy, we can have a real conversation
 - Deceptive combination of the above
     
** Copyleft comic (panel 1)
   :PROPERTIES:
   :BEAMER_envargs: [shrink = 11]
   :END:

Inspired by a true story.

:                COPYLEFT COMIC
:                by Christopher Allan Webber
:                +-----------------------------+
:                |  Don't use that copyleft    |
:                |  license!  It's non-free!   |
:                |  It destroys your freedoms! |
:                |     /                       |
:                |   , ,               , .     |
:                |   O o               o O     |
:                |  \ C /               ~      |
:                |   '|'               /|\     |
:                |                 /           |
:                |   Oh no!  I like free!      |
:                |     Why isn't it free?      |
:                |                             |
:                +-----------------------------+

** Copyleft comic (panel 2)
   :PROPERTIES:
   :BEAMER_envargs: [shrink = 11]
   :END:

:                +-----------------------------+
:                |  I can't use it in this     |
:                |  proprietary program        |
:                |  with my proprietary        |
:                |  license!                   |
:                |     /                       |
:                |   \ /               , .     |
:                |   O o               o O     |
:                |  \ C /             __c      |
:                |   '|'                |\     |
:                |                 /           |
:                |  But your license is even   |
:                |  more restrictive point for |
:                |  point, and forbids even    |
:                |  basic distribution and     |
:                |  modification!              |
:                |                             |
:                +-----------------------------+

** Copyleft comic (panel 3)
   :PROPERTIES:
   :BEAMER_envargs: [shrink = 11]
   :END:

:               +-----------------------------+
:               |  What are you, freetarded?  |
:               |     /                       |
:               |                             |
:               |   \ /               - _     |
:               |   O o               o O     |
:               |   c                  ~      |
:               |  <'|'>              /|\     |
:               |                             |
:               +-----------------------------+
: 
: Copyleft comic released into the public domain
: via CC0 by Christopher Allan Webber.
: Remix & Reuse as seen fit.
: http://creativecommons.org/publicdomain/zero/1.0/

(PS: I don't endorse the use of the term "freetarded".)

** A few more words on copyleft (on support)

But I think copyleft is useful still!
 - Copyleft is *regulation* of the commons
 - Maybe not necessary, or optimal even!, for web libraries as much
 - But definitely for web applications
 - Contrast: Github article "open source almost everything"
 - But why *not* everything?  Why stop right before user freedom?
 - I care about user freedom, not just developer freedom
 
** And a final quote

#+BEGIN_QUOTE
"If you're waving your finger at me over the edge of your macbook about
copyleft being nonfree while committing to your GitHub account
in-between working on your software as a service web application and
the game you're working for the iOS app store, sorry, but I'm not
going to take your 'copyleft is nonfree' argument seriously." -- Me
#+END_QUOTE

(... I hope I didn't just alienate my audience!)

I like non-copyleft licenses *and* copyleft licenses... I think it's a
matter of strategy.

* Further demonstrations

** Time to show some things off!

 - http://mediagoblin.org/
 - http://mg.wandborg.se/u/joar/m/sintel/
 - http://mg.wandborg.se/u/joar/m/tove-swinging/
 - http://mediagoblin.com/u/cwebber/m/exuberant-gnu-head/
 - Show some psycho stuff from http://mediagoblin.com/u/cwebber/ or
   something :)

** We have some other misc cool features too

 - OpenStreetMap integration
 - Attachment support
 - Commenting
 - Creative Commons support
 - Theming
 - The foundations of plugin support
 - and stuff

** Show off theming!

Let's test running a theme or so.

* The technology

** The technology: python

Specifically, "Django minus django"

 - Routes
 - WebOb
 - Jinja2
 - SQLAlchemy
 - WTForms

(/NOT/ "we had django and replaced it with bits", actually the reverse...)

** Look familiar? (views)
   :PROPERTIES:
   :BEAMER_envargs: [shrink = 11]
   :END:

#+BEGIN_SRC python
@get_user_media_entry
@uses_pagination
def media_home(request, media, page, **kwargs):
    """
    'Homepage' of a MediaEntry()
    """
    if ObjectId(request.matchdict.get('comment')):
        pagination = Pagination(
            page, media.get_comments(
                mg_globals.app_config['comments_ascending']),
            MEDIA_COMMENTS_PER_PAGE,
            ObjectId(request.matchdict.get('comment')))
    else:
        pagination = Pagination(
            page, media.get_comments(
                mg_globals.app_config['comments_ascending']),
            MEDIA_COMMENTS_PER_PAGE)

    comments = pagination()

    comment_form = user_forms.MediaCommentForm(request.POST)
#+END_SRC

** Look familiar? (models)
   :PROPERTIES:
   :BEAMER_envargs: [shrink = 12]
   :END:

#+BEGIN_SRC python
class User(Base, UserMixin):
    """
    TODO: We should consider moving some rarely used fields
    into some sort of "shadow" table.
    """
    __tablename__ = "core__users"

    id = Column(Integer, primary_key=True)
    username = Column(Unicode, nullable=False, unique=True)
    email = Column(Unicode, nullable=False)
    created = Column(DateTime, nullable=False,
                     default=datetime.datetime.now)
    pw_hash = Column(Unicode, nullable=False)
    email_verified = Column(Boolean, default=False)
    status = Column(Unicode, default=u"needs_email_verification",
                    nullable=False)
    verification_key = Column(Unicode)
    is_admin = Column(Boolean, default=False, nullable=False)
    url = Column(Unicode)
    [...]
#+END_SRC

** Mongo -> SQL

 - After approx 1 year of MongoDB, we're moving to SQLAlchemy
 - Why did we originally choose Mongo?
 - Why are we moving away? (Hint: not because I hate Mongo)
 - What do I think of SQLAlchemy?

** Extensibile media types and processing
   :PROPERTIES:
   :BEAMER_envargs: [shrink = 13]
   :END:

:   ,-------,
:   |       |                                                
:   | MEDIA |                                                
:   |       |                                                 ,--------,
:   '-------'                                    _converted_\ | PUBLIC |
:       |                                       /   media   / | STORE  |
:     submit                                    :             '--------'
:       |                                       :                 :
:       V                                       :                 :
:   .------------.                      ,------------,            :
:   |            |                      |            |            V
:   | submission |------queues--------> | processing |       ,-----------,
:   |    view    |   for processing     |            |       |           |
:   '------------'                      '------------'       |  Gallery  |
:         :                                   ^              |           |
:         :                                   :              '-----------'
:         '           ,---------,             '
:          \_save __\ |  QUEUE  | ___ fetch _/
:            media  / |  STORE  |     media
:                     '---------'

[[http://wiki.mediagoblin.org/Processing]]

** Theoretically scalable

 - We use celery, theoretically scalable!
 - We have extensible "storage types"!  (Including OpenStack Swift /
   Rackspace Cloud Files support)
 - In reality nobody's scaling it yet.

** Getting started

 - Get started on hacking: [[http://wiki.mediagoblin.org/HackingHowto]]
 - Get started on running an instance: http://docs.mediagoblin.org/
 - Not too hard!  But if you get stuck, ask for help!
   #mediagoblin on irc.freenode.net

* The future?

** Federation

Let's talk about OStatus!

 - Okay, we haven't done this yet
 - http://ostatus.org/
 - Technically a meta-standard
 - Already in use by the fine folks at StatusNet (identi.ca) and
   Diaspora (mostly)
 - Many of the standards developed at Google.
 - I hope we see federation from Google Plus some day but I'm not
   holding my breath.
 - We're planning a library called Kuneco ("coo-neh-tsoh")

** Other things coming soon

 - Plugin infrastructure and more plugins
 - An API!  Going to be paid for by the Icelandic government!  YESSSS!

** Hopefully a lot more deployments

 - One of the major things we need: users and deployments!
 - We've mostly focused on the hacker community while we make sure
   that the software is in a good state
 - Over the next year, we should start focusing on a more general
   public though
 - Are you or your business interested in a mediagoblin instance or
   features?  Hire me!  :)

** We need your help!

 - When it comes to web development, FOSS is the default
 - BUT!  We're seriously lacking in real web applications
 - There's a lot more things to come, but we need your help to make
   them happen
 - It's free software!  You could even start a business on it!
 - We're suuuuper friendly!  Join #mediagoblin on irc.freenode.net!

* Thanks

** We have stickers!

#+begin_latex
\begin{center}\includegraphics[height=0.82\textheight]{../static/chris_and_rob_stickers.jpg}\end{center}
#+end_latex

Thanks for the awesome work on gstreamer, Rob McQueen!

** Get in contact!

*** Contact me!                                                     :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :END:

 - Come up and say hello!
 - Send me an email, or add me to Jabber! [[mailto:cwebber@dustycloud.org][cwebber@dustycloud.org]]
 - Stalk me!  All my contact info!  [[http://dustycloud.org/contact/]]

*** Join our project!                                               :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :END:

 - [[http://mediagoblin.org/join/]]
 - Join *#mediagoblin* on *irc.freenode.net* and say hello!
