* Hello

[[../../guix/static/caminandes-on-mediagoblin.png]]

 - [[http://mediagoblin.org/][MediaGoblin]]
 - Pump.io.
   - "It's federated!"
   - https://github.com/e14n/pump.io/blob/master/API.md
   - https://identi.ca/cwebber
   - Hello world from Pumpa!

And we have Pump.io and MediaGoblin talking to each other

 - http://dustycloud.org/misc/mediagoblin_federation_first_demo.png

* ActivityPub

https://www.w3.org/TR/activitypub/

 - Based on ActivityStreams
 - Client to server!
 - Server to server! (LDN compatible!)

* Demo time

 - Pubstrate: a server implementation!
 - Soci-el: a client!
 - Can show off as2 tooling or rendering those activities if people like ;)
