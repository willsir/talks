# -*- mode: org; org-html-inline-images: nil; -*-

#+TITLE:     Federation and Python
#+AUTHOR:    Christopher Allan Webber
#+EMAIL:     cwebber@dustycloud.org
#+DATE:      2015-01-31 Sat

#+OPTIONS: reveal_center:t reveal_progress:t reveal_history:nil reveal_control:t
#+OPTIONS: reveal_mathjax:t reveal_rolling_links:t reveal_keyboard:t reveal_overview:t num:nil
#+OPTIONS: reveal_width:1200 reveal_height:800
#+OPTIONS: toc:nil
#+REVEAL_MARGIN: 0.06
#+REVEAL_MIN_SCALE: 1
#+REVEAL_MAX_SCALE: 2.5
#+REVEAL_TRANS: cube
#+REVEAL_THEME: night
#+REVEAL_HLEVEL: 2
#+REVEAL_HEAD_PREAMBLE: <meta name="description" content="Federation and Python">


* Introduction
** Who am I?

#+REVEAL_HTML: <img style="float: right;" src="../../network_freedom/static/libreplanet_toon_chris.png" />

# #+ATTR_REVEAL: :frag roll-in
 - Lead MediaGoblin developer!
 - Free software and free culture activist
   - MediaGoblin co-founder / lead dev
   - Now: full time MediaGoblin
   - Formerly: CC, PCF, other things
 - Working on W3C Social WG for federation standard

** What's MediaGoblin?

#+REVEAL_HTML: <img style="height: 550px;" src="../../mediagoblin/static/mediagoblin_mascot.png" />

MediaGoblin is a free software media publishing system anyone can use
and run!

But...

** Decentralization comes with a challenge

#+REVEAL_HTML: <img style="height: 550px;" src="../static/federation-user_adds_image.png" />

Say you uploaded an awesome photo of a mountain on your MediaGoblin
instance...

*** How to share with friends on other servers?

#+REVEAL_HTML: <img style="height: 550px;" src="../static/federation-unsynced.png" />

*** Federate it!

#+REVEAL_HTML: <img style="height: 550px;" src="../static/federation-synced.png" />

** What is federation?

#+REVEAL_HTML: <img style="height: 600px;" src="../../network_freedom/static/purple_federation.png" />


#+BEGIN_NOTES
Kind of a loose term, but it really means interconnecting separate
servers.

For our purposes: if you're going to tear down that centralization
wall, you need a way to bring the same experiences they can have on a
centralized server.
#+END_NOTES


** Why do you care?

#+REVEAL_HTML: <img style="border: none; height: 390px;" src="../../network_freedom/static/nsa-eagle-mildalterations.png" />

The present reality of network freedom isn't pretty...

 - Corporate control of the web
 - NSA spying
 - Freedom for developers, but not for users

So, let's build something better :)

** What does federation mean for MediaGoblin?

# Various activities need to be sync'ed across servers

Mostly federating social activities:

 - Subscribing to someone else's media uploads
 - Commenting
 - Favoriting
 - Note: doesn't necessarily mean "mirroring" media!
   (though maybe a "diskgobbling" plugin could be added)

** How might you use it?

Propagating most "activities" across servers:

 - Sending messages between users privately
 - Posting messages publicly
 - Scheduling calendar events
 - Posting media

* A history of federation

** Pre-web federation

Federation isn't a new concept.

Some older federation standards:

#+ATTR_REVEAL: :frag roll-in
 - Email
 - XMPP/Jabber
 - OpenID  (So, not just sharing data)

** The web in the present

#+REVEAL_HTML: <img style="border: none; height: 570px;" src="../static/nonfree/Founding_Species_of_the_Federation.png" />

Great image from Michał 'Rysiek' Woźniak on active, community-engaged
federation projects.  Except, add Pump.io!

** The Original Series

#+REVEAL_HTML: <img style="border: none; height: 570px;" src="../static/nonfree/star_trek_the_original_series_season_2_10.jpg" />

As long as we're making Star Trek metaphors...

#+BEGIN_NOTES
Totally valid
#+END_NOTES


*** Original cast

# #+ATTR_REVEAL: :frag roll-in
 - Main cast:
   - StatusNet (now GNU Social): OStatus
   - Diaspora: (OStatus, eventually, sorta?)
   - Friendica: Just about everything (incl OStatus)
 - Recurring characters:
   - BuddyCloud (using XMPP on backend)
   - RStatus (Ruby OStatus microblog implementation)
   - Elgg
   - Others I'm sure I've forgotten to mention and will be
     chastized for

*** OStatus

# TODO: OStatus logo here

#+ATTR_REVEAL: :frag roll-in
 - Several authors, though most prominently Evan Prodromou
 - The most "successful" of the Original Protocols
 - Kind of a meta-protocol, weaving together:
   - Webfinger for identity
   - Atom + Activitystreams 1.0 XML representation
   - PubSubHubbub to push out public updates
   - Salmon for private notifications / messaging
 - A good start, but...

*** OStatus (cotd)

#+ATTR_REVEAL: :frag roll-in
 - A big meta-spec means much more to learn
 - Salmon is hard to understand / implement right, private messaging
   never really worked
 - Non-protocol "issues": flagship StatusNet site "identi.ca" grew
   hard to scale, hard to fund with sponsor company gone
 - Evan Prodromou started working on Pump.IO and the Pump API during
   shift
 - OStatus status:
   - Still used by some key players
   - But protocol design itself stagnant

#+BEGIN_NOTES
OStatus protocol work is pretty much stagnant at this point, though
several communities actively using the protocol.

MediaGoblin was going to originally use OStatus, but... now we come
to:
#+END_NOTES

** The Next Generation

#+REVEAL_HTML: <img style="height: 550px;" src="../static/nonfree/tng_homesoil004.jpg" />


*** The cast

 - Interesting stuff at:
   - Pump.IO: the Pump API  (by StatusNet's founder)
   - Red Matrix: Zot  (by Friendica's founder)
   - Mayyyyyybe Tent.io
 - Coming up in next season:
   - The W3C Social WG

*** Some similarities

 - Exchanging json activities to various endpoints seems common
 - Private delivery worked in from the start

*** Focus: Pump.io and the Pump API

MediaGoblin decided to follow Pump for its federation work.  But why?

 - Easy to understand: 
   Mostly just pushing json snippets from a few simple endpoints
 - Led by Evan Prodromou, who has done much leadership here
 - Local expertise: PyPump contributor part of the MediaGoblin project

*** More pump.io tech

 - Posting JSON encoded Activitystreams 1.0 objects
 - Each "user" has inbox/outbox endpoints
 - Webfinger for email-like identity lookup
 - OAuth for authentication between servers

That's "mostly it", see more at the
[[https://github.com/e14n/pump.io/blob/master/API.md][Pump API spec document]]

*** What does a Pump post look like?

#+BEGIN_SRC python
{
    "id": "http://coding.example/api/activity/bwkposthw",
    "actor": {
        "id": "acct:bwk@coding.example",
        "displayName": "Brian Kernighan",
        "objectType": "person",
        "url": "http://coding.example/bwk"
    },
    "verb": "post",
    "object": {
        "id": "http://coding.example/api/note/helloworld",
        "content": "Hello, World!"
        "objectType": "note"
    },
    "published": "1973-01-01T00:00:00"
}
#+END_SRC

Subject, Predicate, Object!

It's pretty simple, easy to unerstand, and solid tech.

*** But is it ready? (p. 1)

#+REVEAL_HTML: <img style="height: 600px;" src="../static/pump_web_and_pumpa.png" />

#+BEGIN_NOTES
Web api sucks, "but the web api is just one possible client"

Good point, but not an easy introduction to family and friends
#+END_NOTES

*** But is it ready? (p. 2)

#+REVEAL_HTML: <img style="height: 550px;" src="../static/nonfree/skinofevil.jpg" />

We're in "season one": good foundations, but a hard sell to newcomers.

#+BEGIN_NOTES
Basically, I think we're on season 1 of Next Generation.  We've got
Picard, but if this is all we have, it's going to be hard to get
people into the series.

Luckily, we've got good foundations.
#+END_NOTES

** A fractured federation

The protocol /is/ the glue of federation here, and yet we don't seem
to agree on what protocol to use...

We now have a smattering of incompatible protocols:

 - OStatus
 - Zot
 - XMPP
 - Pump
 - Tent

** Is a better future possible?

# Public domain "future" inventions images

# http://publicdomainreview.org/collections/a-journey-in-other-worlds-a-romance-of-the-future-1894/
# http://publicdomainreview.org/collections/leaving-the-opera-in-the-year-2000/

# http://publicdomainreview.org/collections/space-colony-art-from-the-1970s/ <- us gov public domain

#+REVEAL_HTML: <img style="height: 600px;" src="../static/gerard_oneill_spacecolony.jpeg" />


*** W3C Social Working Group!

# TODO: Image of the Social WG homepage

#+REVEAL_HTML: <img style="height: 600px;" src="../static/socialwg_wiki_screenshot.jpg" />

Let's make this shit official

*** Status

 - Standardizing on Activitystreams 2.0 (currently in working draft, but
   solidifying)
 - Thorough analysis of requirements from other social networks
 - About to start standardizing "Social API" (client to server
   communication)
 - Then on to formalize federation (server to server communication)
 - Probably will be very Pump-like... things seem to be moving in that
   direction.

* What's a pythonista to do?

** PyPump

 - Object oriented
 - Pythonic
 - Clear encapsulation of activities, etc
 - Simplifies auth

*** How about a real demo!

** How the Python world can help

 - Contribute / use PyPump
 - Get involved in implementing federation into your web applications
   - The Pump API
   - What comes out of the Social WG
 - Help us build fork of PyPump that migrates towards Social WG
   standardization
 - Interested in helping build a reference implementation of the
   Social WG spec?  We should talk!

* Thanks

** Credits

 - Space colony art by NASA Ames Research Center, US Gov public domain
   http://settlement.arc.nasa.gov/70sArt/art.html

** Credits (fair use!)

Lots of pictures from Star Trek, which is proprietary and by Universal
Pictures, but fair use and stuff.

Sources:
 - http://asena42.blogspot.com/2011/08/live-long-and-prosper.html
 - http://tng.trekcore.com/gallery/albums/s1/1x17/homesoil004.jpg
 - https://i.imgur.com/5VCDtNw.jpg
 - https://startrektherewatch.files.wordpress.com/2014/11/skinofevil_hd_386.jpg?w=1180&h=885

Plus this thing by Rysiek:
 - http://events.ccc.de/congress/2014/wiki/Assembly:The_Federation

** Contact info / Questions?

#+REVEAL_HTML: <img style="border: none; height: 500px;" src="../static/nonfree/spock_lightbulb_laser.jpg" />

This talk CC BY-SA 4.0 International

 - *MediaGoblin:* http://mediagoblin.org/
 - *Me:* http://dustycloud.org/
