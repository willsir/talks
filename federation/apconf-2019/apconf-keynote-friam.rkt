#lang slideshow

;; alan's redo
;; - where's where AP came from
;;   - implementations, etc (1 slide)
;;   - serge: make past and present up to 5 slides
;; - we recognize certain problems
;; - what I want to talk about is what AP could make the world a
;;   better place (and that's a keynote)
;; - maybe put the video in the middle instead
;; - put key points on the conclusions slide
;;   picture and 10 words or less
;;    - ocappub
;;    - datashards

;; give people time
;; give fewer slides than you think you need
;; shuf some of the slides till after the conclusions
;; 

;; markm sugestion: break the talk into multiple 15 minute talks???
;;   but then you need a real psychological break between the talks
;; put a story or joke every 15 to 20 minutes

;; TODO: Talk about point that Pandemic wouldn't be appealing
;;   if there was a *rule* that you should hug, but it's emergent
;;   behavior
;; TODO: But then, "how can you plan for emergent behavior":
;;   that's where you introduce the habitat story,
;;   talk about people being able to move *between* worlds
;;     Add board game *shelf* to show the ability to pick it up


(require racket/draw
         slideshow/text
         pict/code)

;; Set the font
#;(current-main-font (cons (make-object color% 42 40 40)
                         "PT Sans"))

(current-main-font (cons (make-object color% 42 40 40)
                         "Inconsolata"))

#;(current-main-font (cons (make-object color% 237 237 237)
                         "Inconsolata"))

#;(set-page-numbers-visible! #f)

(define default-slide-assembler
  (current-slide-assembler))

;; Set a light cream background
(current-slide-assembler
 (lambda (s v-sep c)
   (define width
     (+ (* margin 2) client-w))
   (define height
     (+ (* margin 2) client-h))
   (ct-superimpose
    (inset (filled-rectangle width height
                             #:color (make-object color%
                                                  ;; 50 47 47
                                                  237 237 237)
                             #:draw-border? #f)
           (- margin))
    (default-slide-assembler s v-sep c))))

(define (scaled-image filename [height-adjust 0])
  (scale-to-fit (bitmap filename)
                client-w (- client-h height-adjust)))

(define (ascii-art str [size 20])
  (apply vl-append
         (for/list ([line (string-split str "\n")])
           (text line 'modern size))))

(define (gesso pict [bg-color "white"] [inset-size 15])
  (define inset-p
    (inset pict inset-size))
  (cc-superimpose (filled-rectangle (pict-width inset-p)
                                    (pict-height inset-p)
                                    #:color "white"
                                    #:draw-border? #f)
                  inset-p))



;; - Federation and ActivityPub

(slide
 (bitmap "../../federation/static/ActivityPub-logo.png")
 (bold (titlet "ActivityPub: Past, Present, Future"))
 (t "Christopher Lemmer Webber")
 (t "https://dustycloud.org/")
 (small (t "Fediverse: https://octodon.social/@cwebber"))
 (small (t "Birdsite: https://twitter.com/dustyweb"))
 (small (t "Original fediverses (email/xmpp): cwebber@dustycloud.org")))


;; - Introduction
;;   - Wow!  ActivityPub Conf 2019!
;;   - Let's thank the organizers and volunteers<
;;   - This is a community; treat each other with respect
;;   - Just in case you didn't know what ActivityPub is

(slide
 ;; TODO: ActivityPub Conf logo
 #:title "Welcome to ActivityPub Conf 2019!"
 'next
 (item "Let's thank the organizers and volunteers")
 'next
 (item "Let's thank the venue")
 'next
 (item "Treat each other with respect"))

(slide
 (big (big (t "Understanding ActivityPub"))))

(slide
 #:title "What *is* ActivityPub?"
 (para "Just in case you didn't know...")
 'next
 (item "Server-to-Server protocol (federation!)")
 'next
 (item "Client-to-Server protocol (mobile, desktop, web clients)")
 'next
 (item "Choose one, or choose both!"
       "Once you have one, the other is easy.")
 ;; 'next
 ;; (item "Uses ActivityStreams as its vocabulary")
 ;; 'next
 ;; (item "Uses JSON-LD as its serialization")
 )

(slide
 #:title "This is Alyssa, an actor!"
 ; (titlet "This is Alyssa, an actor!")
 (bitmap "../../federation/static/activitypub-illustration/tutorial-1.png")
 (scale
  (codeblock-pict "{\"@context\": \"https://www.w3.org/ns/activitystreams\",
 \"type\": \"Person\",
 \"id\": \"https://social.example/alyssa/\",
 \"name\": \"Alyssa P. Hacker\",
 \"preferredUsername\": \"alyssa\",
 \"summary\": \"Lisp enthusiast hailing from MIT\",
 \"inbox\": \"https://social.example/alyssa/inbox/\",
 \"outbox\": \"https://social.example/alyssa/outbox/\",
 \"followers\": \"https://social.example/alyssa/followers/\",
 \"following\": \"https://social.example/alyssa/following/\",
 \"liked\": \"https://social.example/alyssa/liked/\"}")
  1.5))

(slide
 #:title "An overview of ActivityPub"
 (bitmap "../../federation/static/activitypub-illustration/tutorial-2.png"))

(slide
 #:title "Alyssa's client =POST=> Alyssa's outbox"
 (bitmap "../../federation/static/activitypub-illustration/tutorial-3.png")
 (scale
  (codeblock-pict "{\"@context\": \"https://www.w3.org/ns/activitystreams\",
 \"type\": \"Note\",
 \"to\": [\"https://chatty.example/ben/\"],
 \"attributedTo\": \"https://social.example/alyssa/\",
 \"content\": \"Say, did you finish reading that book I lent you?\"}")
  1.5))

(struct card
  (label body))

;; Render a series of cards all to have the same width and height
(define (render-cards . cards)
  (define max-height-body
    (apply max
           (map (lambda (card)
                  (pict-height (card-body card)))
                cards)))
  (define card-contents
    (for/list ([card cards])
      (let* ([body
              (card-body card)]
             [inset-body
              (inset body
                     (- max-height-body (pict-height body))
                     0)])
        (vc-append 10
                   (text (card-label card) '(bold . "Inconsolata") 18)
                   inset-body))))
  (define w
    (+ (apply max (map pict-width card-contents))
       20))
  (define h
    (+ (apply max (map pict-height card-contents))
       20))
  (define card-face
    (filled-rounded-rectangle
     w h -.15
     #:color "white" #:border-color "gray"
     #:border-width 5))
  (for/list ([card-content card-contents])
    (cc-superimpose card-face card-content)))


(define note-file-icon
  (ct-superimpose
   (file-icon 70 85 "honeydew" #t)
   (inset (vc-append 10
                     (text "Note" "Inconsolata" 16)
                     (text "\"Did you...\"" "Inconsolata" 8))
          10)))

(define create-file-icon
  (ct-superimpose
   (file-icon 70 85 "bisque" #t)
   (inset (vc-append 10
                     (text "Create" "Inconsolata" 16)
                     (cellophane (scale note-file-icon 1/2)
                                 .5))
          10)))


(define addressing-arrow
  (colorize (arrow 40 0)
            (make-color 170 180 120)))

(define ap-example-cards
  (render-cards
   (card "Actor"
         (scale (bitmap "../../federation/static/activitypub-illustration/just-alyssa.png")
                1/2))
   (card "Activity"
         create-file-icon)
   (card "Object"
         note-file-icon)))

(slide
 #:title "Server wraps it in a Create..."
 (scale
  (codeblock-pict "{\"@context\": \"https://www.w3.org/ns/activitystreams\",
 \"type\": \"Create\",
 \"id\": \"https://social.example/alyssa/posts/a29a6843\",
 \"to\": [\"https://chatty.example/ben/\"],
 \"actor\": \"https://social.example/alyssa/\",
 \"object\": {\"type\": \"Note\",
            \"id\": \"https://social.example/alyssa/posts/49e2d03d\",
            \"attributedTo\": \"https://social.example/alyssa/\",
            \"to\": [\"https://chatty.example/ben/\"],
            \"content\": \"Did you finish reading that book I lent you?\"}}")
  1.5)
 'next
 (t "An actor always performs an activity on an object...")
 (apply hc-append
        30 ap-example-cards)
 'next
 (t "... with email-like addressing.")
 (car (render-cards
       (card "To"
             (scale (bitmap "../../federation/static/activitypub-illustration/just-ben.png")
                    1/2)))))

(slide
 #:title "Federation: Alyssa's server POSTs to Ben's Inbox"
 (bitmap "../../federation/static/activitypub-illustration/tutorial-4.png")
 (t "Take note!")
 (t "Federation *is* POSTing to an inbox!"))

(slide
 #:title "A simplified view"
 (bitmap "../../federation/static/activitypub-illustration/tutorial-5.png")
 (t "And Ben can GET from his inbox to see recent posts!"))

(slide
 #:title "Let's see that again!"
 (bitmap "../../federation/static/activitypub-illustration/tutorial-2.png"))

(define actor-types
  (list "Application"
        "Group"
        "Organization"
        "Person"
        "Service"))

(define activity-types1
  (list "Accept"
        "Add"
        "Announce"
        "Arrive"
        "Block"
        "Create"
        "Delete"
        "Dislike"
        "Flag"
        "Follow"
        "Ignore"
        "Invite"
        "Join"
        "Leave"))

(define activity-types2
  (list "Like"
        "Listen"
        "Move"
        "Offer"
        "Question"
        "Reject"
        "Read"
        "Remove"
        "TentativeReject"
        "TentativeAccept"
        "Travel"
        "Undo"
        "Update"
        "View"))

(define object-types
  (list "Article"
        "Audio"
        "Document"
        "Event"
        "Image"
        "Note"
        "Page"
        "Place"
        "Profile"
        "Relationship"
        "Tombstone"
        "Video"))

(let ([vertical-words
       (lambda (words)
         (apply vc-append
                (map (lambda (word)
                       (small (t word)))
                     words)))])
  (slide
   #:title "ActivityStreams is AP's social vocabulary"
   (ht-append 150
              (vc-append
               20
               (first ap-example-cards)
               (vertical-words actor-types))

              (vc-append
               20
               (second ap-example-cards)
               (hc-append 10
                (vertical-words activity-types1)
                (vertical-words activity-types2)
                ))

              (vc-append
               20
               (third ap-example-cards)
               (vertical-words object-types)))
   (italic (t "Extensible with JSON-LD, too!"))))

(slide
 (t "Read the Overview section of the ActivityPub spec!")
 (small (t "This stuff is easier than you think!")))


#;(slide
 (scaled-image "../../federation/static/w3c-activitypub-recommendation.png"))

;; - Past

(slide
 (big (big (t "The Past"))))

;;   - StatusNet / OStatus
;;   - MediaGoblin
;;   - I guess we'll do OStatus?
;;   - Pump.IO & Pump API
;;   - Fractured federation
;;   - Erin Shepherd transformed Pump API into "ActivityPump"
;;   - Evan Prodromou announces he's co-chairing Social CG
;;   - "This is our one chance!"  Attempt to rally the troops
;;     - "You're never going to get these people to cooperate."
;;     - But what's the alternative?   
;;   - "We'll just show up for an hour or so a week"
;;     - Next thing we know, Jessica and I are co-editors
;;   - 3 years pass by
;;     - Show photos from the CG
;;     - Not going to lie to you: standardizing ActivityPub was brutal
;;     - Also give credit to Amy Guy
;;   - ActivityPub almost ran out of time.  "Have I wasted 3 years of my
;;     life?"
;;   - Mastodon adopts ActivityPub, we get an extension
;;   - ActivityPub reaches CR!
;;   - ... with "holes in the spec"
;;   - But the what's in there is good!  (Mostly)

(slide
 #:title "Pre-ActivityPub: A fractured federation"
 (para "A smattering of incompatible protocols:")
 (item "OStatus")
 (item "Zot")
 (item "XMPP")
 (item "Pump")
 (item "Tent"))

(slide
 #:title "W3C Social Working Group!"
 (scaled-image "../static/socialwg_wiki_screenshot.jpg" 200)
 (t "Let's make this official, yo"))

(slide
 (scaled-image "../static/2015-03-18-w3c-f2f.jpg" 200)
 (para "Chris and Jessica: \"We'll just show up an hour a week to make sure"
       "things are on track...\""))

#;(slide
 (para "TODO: 3 years of SocialWG photos here"))

(slide
 #:title "The SocialWG clock is ticking"
 (item "It doesn't look like we'll make it in time")
 'next
 (item "Depression: \"I have wasted 3 years of my life\"")
 'next
 (item "Mastodon picks up ActivityPub")
 (subitem "An explosion of implementations!")
 (subitem "We get an extension!"))

(slide
 (italic (t "* OVER THREE YEARS OF EXHAUSTING WORK LATER *"))
 (scaled-image "../static/w3c-activitypub-recommendation.png")
 (t "Whoohoo!")
 (small (t "(it was a lot more than an hour a week...)")))

(slide
 #:title "ActivityPub: a labor of love from many"
 'alts
 (list
  (list
   (scaled-image "../static/ap-spec-top.png" 100))
  (list
   (scaled-image "../static/ap-spec-acknowledgements.png" 100))))


;; - Present

(slide
 (big (big (t "The Present"))))

;;   - Fediverse at large picking up ActivityPub
;;   - We're done... right?
;;   - "Let's codify the holes in the spec as-is!"
;;   - But there are a lot of problems to solve...
;;   - Spritely

(slide
 #:title "We're no longer so small..."
 'next
 (item "Over 2 million (registered) users?")
 'next
 (item "Over 50 implementations?")
 'next
 (para "I used to have slides trying to keep track but I can't anymore"))

(slide
 #:title "Success! We're done... right?"
 ;; 'next
 ;; (item "Many issues remain")
 ;; 'next
 ;; (subitem "Nodes go down and users are unhappy")
 ;; 'next
 ;; (subitem "An uptick in spam and abuse")
 ;; 'next
 ;; (subitem "We don't have very \"rich\" interactions")
 ;; 'next
 ;; (item "Protocol nerdery")
 ;; (subitem "Remove sharedInbox")
 ;; (subitem "Message serialization / extensibility improvements")
 ;; 'next
 ;; (item "Spritely: mad science lab for the fediverse's future")
 'next
 (t "Well..."))

;; - Future

(slide
 (big (big (t "The Future")))
 (t "(... is a place where we solve today's problems)"))

;;   - Survive content going down
;;   - Survive servers going down

(slide
 #:title "Issue: nodes go down"
 'next
 (item "Users lose access to their content :(")
 'next
 (item "Friends of those users can't reference their old conversations, either :(")
 'next
 (subitem "This doesn't happen in email!")
 'next
 (item "Have to restart all your social connections")
 'next
 (subitem "Opens your friends up to phishing attacks!"
          "Who is the \"real you\"?"))

(define (inset-t text [frame? #f])
  (if frame?
      (frame (inset (t text) 1)
             #:color "DarkGreen"
             #:line-width 3)
      (inset (t text) 1)))

;; TODO: Cut out a significant part of datshards

(slide
 (scaled-image "../../datashards/static/Datashards-logo.png" 500)
 'alts
 (list
  (list
   (hc-append
    (inset-t "Secure")
    (t ", ")
    (inset-t "distributed storage")
    (t " ")
    (inset-t "primitives for the web")))
  (list
   (hc-append
    (inset-t "Secure" #t)
    (t ", ")
    (inset-t "distributed storage")
    (t " ")
    (inset-t "primitives for the web"))
   (item "Private; only intended recipients can see messages")
   (item "Safer for node operators to help network")
   (item "Secure against size-of-file-attacks"))
  (list
   (hc-append
    (inset-t "Secure")
    (t ", ")
    (inset-t "distributed storage" #t)
    (t " ")
    (inset-t "primitives for the web"))
   (item "Many different \"stores\"")
   (subitem "A \"Secure Data Hub\" web store!")
   (subitem "Your local filesystem!")
   (subitem "USB keys!")
   (subitem "Distributed over a gossip network/DHT!")
   (item "Nodes can host content, but don't know what it is"))
  (list
   (hc-append
    (inset-t "Secure")
    (t ", ")
    (inset-t "distributed storage")
    (t " ")
    (inset-t "primitives for the web" #t))
   (item "Meant to be foundational, the way http(s) is foundational for"
         "live content")
   (item "Two new URI schemas:")
   (subitem "idsc: Immutable, unchanging data")
   (subitem "mdsc: Mutable, updateable data")
   (item "Data can have the same name, but can live in many places"))
  (list
   (t "But what does Datashards solve?")
   'alts
   (list
    (list
     (item "Content which can survive nodes going down")
     (subitem "Use datashards URIs as id instead of http(s)")
     (subitem "Demo of this exists: see Spritely Golem"))
    (list
     (item "Actors/user accounts which can survive nodes going down")
     (subitem "Make the actor id an mdsc URI; point inbox at your current node")
     (subitem "That node goes down? Change inbox, push update out to the network.")
     (subitem "(For all intents and purposes, this is a DID?)"))
    (list
     (item "Stop punishing content authors for successful content")
     (subitem "Why should content creators pay for bandwidth?")
     (subitem "Shouldn't we help them?"))))))


;;   - Revisiting client/server
;;     - Client/server part of spec
;;     - Peer-to-peer'ification

(slide
 #:title "Issue: Revisiting client-to-server"
 (item "\"I'm making the ActivityPub server for $X experience!\"")
 'next
 (subitem "Is this actually good for users though?")
 ;; TODO: distinguishing that we're not talking about centralization
 ;;   multiple *persons*
 (subitem "Why have 20 accounts when you could have one?")
 (subitem "Where are the general-purpose servers?")
 'next
 (item "ActivityPub ships a client-to-server API.  Why is nobody"
       "using it?"))

(slide
 #:title "We aren't really decentralized"
 (t "DNS and SSL CAs"))

(slide
 #:title "Maybe P2P > C2S"
 (item "What if client-to-server is an antipattern?")
 ;; TODO: Say end-to-end-encryption (confused akarp)
 (subitem "Makes E2E difficult")
 (subitem "Unhealthy power dynamics")
 (subitem "Makes self-hosting difficult")
 'next
 (item "P2P: combine both client and server into one!"))

(slide
 #:title "https -> tor onion services"
 'alts
 (list
  (list
   (scale
    (codeblock-pict "{\"@context\": \"https://www.w3.org/ns/activitystreams\",
 \"type\": \"Person\",
 \"id\": \"https://social.example/alyssa/\",
 \"name\": \"Alyssa P. Hacker\",
 \"preferredUsername\": \"alyssa\",
 \"summary\": \"Lisp enthusiast hailing from MIT\",
 \"inbox\": \"https://social.example/alyssa/inbox/\",
 \"outbox\": \"https://social.example/alyssa/outbox/\",
 \"followers\": \"https://social.example/alyssa/followers/\",
 \"following\": \"https://social.example/alyssa/following/\",
 \"liked\": \"https://social.example/alyssa/liked/\"}")
    1.5))
  (list
   (scale
    (codeblock-pict "{\"@context\": \"https://www.w3.org/ns/activitystreams\",
 \"type\": \"Person\",
 \"id\": \"http://4acth47i6kxnvkewtm6q7ib2s3ufpo5sqbsnzjpbi7utijcltosqemad.onion/\",
 \"name\": \"Alyssa P. Hacker\",
 \"preferredUsername\": \"alyssa\",
 \"summary\": \"Lisp enthusiast hailing from MIT\",
 \"inbox\": \"http://4acth47i6kxnvkewtm6q7ib2s3ufpo5sqbsnzjpbi7utijcltosqemad.onion/inbox/\",
 \"outbox\": \"http://4acth47i6kxnvkewtm6q7ib2s3ufpo5sqbsnzjpbi7utijcltosqemad.onion/outbox/\",
 \"followers\": \"http://4acth47i6kxnvkewtm6q7ib2s3ufpo5sqbsnzjpbi7utijcltosqemad.onion/followers/\",
 \"following\": \"http://4acth47i6kxnvkewtm6q7ib2s3ufpo5sqbsnzjpbi7utijcltosqemad.onion/following/\",
 \"liked\": \"http://4acth47i6kxnvkewtm6q7ib2s3ufpo5sqbsnzjpbi7utijcltosqemad.onion/liked/\"}")
    1.2))
  (list
   (item "Easy to self-host at home (NAT punching)")
   'next
   (item "Secure connection: the name is the encryption key")
   'next
   (item "No DNS + SSL CAs!"))))


;;   - Petnames

(slide
 #:title "But what about webfinger"
 ;; TODO: show contrast
 (scale (codeblock-pict "cwebber@4acth47i6kxnvkewtm6q7ib2s3ufpo5sqbsnzjpbi7utijcltosqemad.onion")
        1.5)
 (blank-line)
 ;; TODO: Make it clearer: we need to give up on webfinger
 (t "Help, I can't remember this (and neither can you!)")
 'next
 (t "Same problem for Datashards-based profiles!"))

;; TODO: Make the rotation to match to the one on wikipedia

(define zt-unlabeled
  "\
         Human Meaningful

                /\\
               /  \\
              /    \\        
             /      \\
            /        \\
           /__________\\

Decentralized       Globally Unique
                 

                          
                      ")

(define zt-personal
  "\
         Human Meaningful

                /\\
               /  \\
 Personal ->  /    \\        
   names     /      \\
            /        \\
           /__________\\

Decentralized       Globally Unique
                 

                          
                      ")

(define zt-dns
  "\
         Human Meaningful

                /\\
               /  \\
 Personal ->  /    \\  <- DNS
   names     /      \\
            /        \\
           /__________\\

Decentralized       Globally Unique
                

                          
                      ")


(define zt-sad
  "\
         Human Meaningful

                /\\
               /  \\
 Personal ->  /    \\  <- DNS
   names     /      \\
            /        \\
           /__________\\

Decentralized   ^   Globally Unique
                |

       Self-Authenticating
           Designators")

(slide
 #:title "Zooko's Triangle"
 'alts
 (list
  (list
   (ascii-art zt-unlabeled))
  (list
   (ascii-art zt-personal)
   (blank-line)
   (italic (t "Eg: \"Chris Webber\"")))
  (list
   (ascii-art zt-dns)
   (blank-line)
   (italic (t "Eg: dustycloud.org")))
  (list
   (ascii-art zt-sad)
   (blank-line)
   (small (italic (t "Eg: 4acth47i6kxnvkewtm6q7ib2s3ufpo5sqbsnzjpbi7utijcltosqemad.onion")))
   (small (small (italic (t "Eg: idsc:0p.x9ZO2FiOy7rtf5bcMoUnU_IeMHTvobiAeH3tcc9W_OE.UF84o-DrREcdP5McSk6YPJJDSzp4h9TEbAi35WXmJPE")))))
  (list
   (ascii-art zt-sad)
   (blank-line)
   (bold (t "Self-Authenticating Designators:"))
   (t "\"My name is how you'll know it's me.\"")
   (small (t "-- Mark S. Miller")))))

(slide
 #:title "The solution: Petname systems"
 (scaled-image "../static/Pidgin_2.0_contact_window.png"
               200))

;;   - Serialization & vocabulary

;;   - Down with sharedInbox!
#;(slide
 #:title "Protocol nerdery issues"
 (t "(unconference topics for tomorrow?)")
 'next
 (item "Improvements to message serialization?")
 'next
 (item "Can we replace sharedInbox with something"
       "that doesn't break ocaps / the actor model?"))

#;(slide
 #:title "Issue: JSON-LD?"
 (item "JSON-LD solves an actual, serious issue:"
       "we need extensibility without ambiguity")
 (subitem "\"run\" a program?")
 (subitem "\"run\" a mile?")
 'next
 (item "That said, it's clearly a source of contention")
 'next
 'alts
 (let ([too-big
        "More thoughts over next year, too big of a topic for this keynote"])
   (list (list (item too-big))
         (list (item too-big (small (small (t "(... \"preserves-ld\")")))))))
 'next
 (item "Unconference session tomorrow?"))

;;   - Rich interactions & games

(slide
 #:title "The lost cyberpunk world of social games"
 'alts
 (list
  (list
   (small
    (para #:align 'center
          "MUDs, MOOs, MUSHes: social spaces with a sense of place"))
   (scaled-image "../../federation/static/mud.png" 150))
  (list
   (small
    (para #:align 'center
          "Habitat: massively multiplayer graphical game... in"
          "1985!"))
   (scaled-image "../../mmose/static/lessonshabitat.gif" 200)
   (small (small (t "https://web.stanford.edu/class/history34q/readings/Virtual_Worlds/LucasfilmHabitat.html"))))
  (list
   (scaled-image "../../federation/static/desiderata.png" 150))))

(slide
 #:title "People want to create"
 'alts
 (list
  (list
   (scaled-image "../../mmose/static/minetest.jpg" 200)
   (small (italic (t "CC BY-SA 3.0, Minetest Team"))))
  (list
   ;; TODO: say host a node
   (item "How many people host a federated social network?")
   ;; TODO: Add versus
   'next
   (item "How many people host a minecraft server?"
         "(How many kids?)"))))

#;(slide
 (scaled-image "../../mmose/static/mud-mockup.png" 300)
 'alts
 (list
  (list
   (para "Game tab: Multiplayer games as social networks "
         "with a sense of place (though you could connect"
         "to multiple realms)"))
  (list
   (para "Social tab: Why not put an ActivityPub social timeline"
         "right in this same interface?"))
  (list
   (para "Ben tab: Direct message with another player"))
  (list
   (para "Can build up a contact list style petnames system"
         "in this application"))
  (list
   (para "Browser tab: A paired down lynx-style browser"
         "and why you'd want it"))))

(define (ec-vid . names)
  (define i-len
    (length names))
  (list
   'alts
   (for/list ([name names]
              [i (in-naturals)])
     (list
      (scaled-image (format "../static/ec-habitats-vid/vlcsnap-2019-08-29-~a.png" name)
                    150)
      (cellophane (t (advance-indicator i-len i))
                  .75)))))

(define (advance-indicator i-len i)
  (define (n-spaces n)
    (list->string (for/list ([i n])
                    #\space)))
  (format "[~a_~a]"
          (n-spaces i)
          (n-spaces (abs (- i-len i 1)))))

(define (ec-text . text)
  (list (apply para #:align 'center text)))

(slide
 #:title "Electric Communities Habitat"
 'alts
 (list
  (list
   (para #:align 'center "P2P, distributed social game... in 1997!")
   (para #:align 'center "Where can I see more???")
   'next
   (para #:align 'center "There's... one video of this thing..."))
  (ec-vid "10h36m39s452"
          "10h38m05s210"
          "10h38m18s760")
  (ec-text "Everyone connected to the network is both a client and server!")
  (ec-vid "10h38m55s876"
          "10h39m00s170"
          "10h39m04s395"
          "10h39m09s488"
          "10h39m13s926"
          "10h39m18s264"
          "10h39m24s533"
          "10h39m28s211"
          "10h39m32s054"
          "11h14m47s329")
  (ec-text "Rich interactions!")
  (ec-vid "10h41m36s768"
          "10h41m40s529"
          "10h41m44s151"
          "10h41m47s636"
          "10h42m00s987"
          "10h42m04s886"
          "10h42m09s010"
          "10h42m12s657"
          "10h42m16s635"
          "10h42m20s017"
          "10h42m23s683"
          "10h44m47s551")
  (ec-text "~Safely run code or use assets from untrusted other users!")
  (ec-vid "10h43m20s932"
          "10h43m31s825"
          "10h43m39s468"
          "10h43m43s400"
          "10h43m51s906"
          "10h43m55s378"
          "10h43m59s182"
          "10h44m03s101"
          "10h44m07s148"
          "10h44m10s863"
          "10h44m14s381"
          "10h49m09s599")
  (ec-text "Anyone can make their own world!")
  (ec-vid "10h45m09s702"
          "10h45m15s409"
          "10h46m38s402"
          "10h45m48s359")
  (ec-text "Different \"Terms of Entry\" for different realms!")
  (ec-vid "10h46m49s104"
          "10h47m03s768"
          "10h46m16s961")
  (ec-text "How did they make this in 1997???"
           "Where can I learn more????")
  #;(ec-vid "10h47m32s501"
          "10h47m58s902"
          "10h48m03s402"
          "10h48m07s705"
          "10h48m11s381"
          "10h48m15s590"
          "10h48m19s072"
          "10h48m23s047"
          "10h48m26s802"
          "10h47m20s388")
  (ec-vid "11h21m57s293")
  ;; TODO: You can't get access to it, not publicly available
  (ec-text "Oh... the project died, the company pivoted,"
           "the code is gone.")))

(slide
 #:title "And yet... its ideas survive!"
 (scaled-image "../../mmose/static/erights.png" 300)
 (para #:align 'center
       "E / erights.org: the most interesting language / website you've never heard of"))

(slide
 #:title "Object Capabilities (ocaps!)"
 (para "Authority not by who you are, but what you hold onto..."
       "\"unforgeable references\"")
 ;; TODO: Remove this detail
 (item "Car keys")
 (item "Google docs links")
 (item "blah blah..."))

(slide
 #:title "Oh hey look a Granovetter Diagram (huh???)"
 (scaled-image "../static/granovetter.png" 400)
 'next
 'alts
 (list
  (list
   (item "Alice knows Bob")
   'next
   (item "Alice knows Carol")
   'next
   (item "Alice sends the message \"foo\" introducing Bob to Carol"))
  (list
   (t "But what does this have to do with social systems?"))))

(slide
 #:title "The Strength of Weak Ties"
 (scaled-image "../static/granovetter-paper-1.png" 150)
 (small (t "Mark S. Granovetter, 1973")))

(slide
 #:title "The Strength of Weak Ties"
 (scaled-image "../static/granovetter-paper-2.png" 150)
 (small (t "Mark S. Granovetter, 1973")))

(slide
 (scaled-image "../static/ocap-in-virtual-environments.png"))

;;   - Spam and abuse


(slide
 (scaled-image "../static/ocappub.png"))

;;   ;; - "Fake news"?

;;   - Be clear about what we can and can't prevent

(slide
 'alts
 (list
  (list
   (t "\"Only prohibit what you can prevent\""))
  (list
   (cellophane (t "\"Only prohibit what you can prevent\"")
               .5)
   'alts
   (list
    (list
     (para #:align 'center
           "Almost, but not quite right...")
     'next
     (item "Things we can't prevent in-protocol but prohibit")
     'next
     (item "Might enforce out-of-protocol")
     (subitem "Social requests")
     (subitem "Social shame")
     (subitem "Legally")
     (subitem "Expulsion from a community/club"))
    (list
     (arrow 100 (/ pi -2))
     (para #:align 'center
           "\"We must not claim we can prevent what we cannot\"")
     'next
     (blank-line)
     (t "To do otherwise would put our users at risk!"))))))

(slide
 (scaled-image "../static/ways-to-harm-someone-online.png"))

(slide
 (scaled-image "../static/ways-to-harm-someone-online-colors.png"))

;;   - Emergent behavior and guiding philosophy

(slide
 #:title "Emergent behavior"
 (scaled-image "../static/conways-game-of-life.png" 100))

(slide
 (scaled-image "../static/Amsterdam_Risk_players.jpg"))

(slide
 (scaled-image "../static/Pandemic_board_game.jpg"))

(slide
 #:title "Can we provide guiding philosophy to our users?"
 (bitmap "../static/EleanorRooseveltHumanRights.png"))

;;   - Fun

(slide
 #:title "Keep it fun!"
 (scaled-image "../static/Fun_in_the_snow.jpg" 100))

;;
;; - Conclusions
;;   - Can we do this while remaining compatible?
;;   - Revolutions are run by those who show up (welcome to it)
;;   - Credits
;;     - contact list photo: https://en.wikipedia.org/wiki/Contact_list#/media/File:Pidgin_2.0_contact_window.png GPL, Pidgin team
;;     - risk https://en.wikipedia.org/wiki/Risk_(game)#/media/File:Amsterdam_-_Risk_players_-_1136_(cropped).jpg
;;     - pandemic https://en.wikipedia.org/wiki/Pandemic_(board_game)#/media/File:Pandemic_board_game.jpg
;;     - Ways to Harm Someone Online: https://docs.google.com/document/d/1ZQDWwh3J6c_RAVlIAPAKI6eRB4HI7HlrBwAJrP8btkQ/edit
;;     - conway's game of life screenshot from https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life
;;     - Universal Declaration of Human Rights https://en.wikipedia.org/wiki/Universal_Declaration_of_Human_Rights#/media/File:EleanorRooseveltHumanRights.png
;;     - Snowball fight https://commons.wikimedia.org/wiki/File:Snowball_fight_at_China.jpg

;;   - Thanks

;; <dustyweb> hi
;; <dustyweb> some time ago you said something useful to me when I was saying that I can't believe people take me seriously
;; <dustyweb> "Revolutions are run by the people who show up"
;; <dustyweb> I hadn't heard it before; it was useful to hear
;; <dustyweb> I'm including the quote in my ActivityPub Conf talk.  I'm not sure if I should attribute it to you or if you were quoting someone or if it's a common phrase
;; <bkuhn> I'm quite sure I didn't  come up with it, but I don't know the source
;; <bkuhn> I am reasonably sure I heard it from Jello Biafra but I think he got it from somewhere else too
;; <bkuhn> Best I can find is this: https://www.barrypopik.com/index.php/new_york_city/entry/the_world_is_run_by_those_who_show_up
;; <bkuhn> I am 80% sure or so that Jello Biafra said the "Revolutions" version in one of his spoken word albums.
;; <dustyweb> cool, thank you
;; <dustyweb> cool, thank you
;; <bkuhn> You could certainly say: "This is an old quote that I first heard from Bradley M. Kuhn"
;; <bkuhn> I'd be cool with that, makes it clear I didn't come up with it but I was the first one to tell you. :L)
;; <bkuhn> It's actually that very quote that gave me the confidence to just try to work for the FSF and become part of software freedom policy.
;; <bkuhn> It's admittedly a certain amount of white-male-privilege, but in my conscious mind, that quote convinced me: "Um, why am I *not* someone who should be able to decide Free Software policy?"
;; <bkuhn> One of my unique abilities is to automatically *care* about anything that crosses my path and wanting to do something about it.
;; <dustyweb> :)

(slide
 (t "\"Revolutions are run by the people who show up.\"")
 (small (t "-- Author unknown (but I heard it from bkuhn)"))
 'next
 (blank-line)
 (t "Have fun at ActivityPub Conf!")
 (t "And welcome to the revolution."))
