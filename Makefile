ORG_REVEAL = \
  mediagoblin/blenderconf_2013/mediagoblin_blender.org \
  network_freedom/fosdem_2014/network_freedom.org \
  network_freedom/libreplanet_2014/state_of_the_goblin.org \
  network_freedom/flourish_2014/state_of_the_goblin.org \
  crowdfunding/seagl_2014/crowdfunding_freesw.org \
  deployment/fosdem_2015/deploying_libreweb.org \
  federation_python/fosdem_2015/federation_python.org \
  federation_python/libreplanet_2015/federation_and_gnu.org \
  user_developer/fosdem_2015/user_developer.org

ORG_REVEAL_HTML = $(patsubst %.org,%.html,$(ORG_REVEAL))

ORG_BEAMER = \
  mediagoblin/oscon_2012/mediagoblin_oscon.org \
  guix/stripe_2016/guix_at_stripe.org

ORG_BEAMER_PDF = $(patsubst %.org,%.pdf,$(ORG_BEAMER))

ORG_HTML = \
  guix/chicagolug_2015/guix_talk.org
ORG_HTML_HTML = $(patsubst %.org,%.html,$(ORG_HTML))

EMACS_SCRIPT = emacs --batch -l install-deps.el -l export.el

.PHONY: all clean beamer-talks reveal-talks serve

all: reveal-talks html-talks

beamer-talks: $(ORG_BEAMER_PDF)

reveal-talks: $(ORG_REVEAL_HTML)

html-talks: $(ORG_HTML_HTML)

$(ORG_REVEAL_HTML): %.html: %.org
	$(EMACS_SCRIPT) --eval '(export-reveal-talk "$^")'

$(ORG_BEAMER_PDF): %.pdf: %.org
	$(EMACS_SCRIPT) --eval '(export-beamer-talk "$^")'

$(ORG_HTML_HTML): %.html: %.org
	$(EMACS_SCRIPT) --eval '(export-html-talk "$^")'

clean:
	-rm -f $(ORG_REVEAL_HTML)
	-rm -f $(ORG_BEAMER_PDF)

serve:
	@echo "Serving on http://127.0.0.1:8080/"
	@python -c "from BaseHTTPServer import HTTPServer; from SimpleHTTPServer import SimpleHTTPRequestHandler; HTTPServer(('127.0.0.1', 8080), SimpleHTTPRequestHandler).serve_forever()" 
