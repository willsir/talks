#lang slideshow

(require racket/draw
         slideshow/text
         pict/code)

;; Set the font
#;(current-main-font (cons (make-object color% 42 40 40)
                         "PT Sans"))

(current-main-font (cons (make-object color% 42 40 40)
                         "Inconsolata"))

#;(current-main-font (cons (make-object color% 237 237 237)
                         "Inconsolata"))

#;(set-page-numbers-visible! #f)

(define default-slide-assembler
  (current-slide-assembler))

;; Set a light cream background
(current-slide-assembler
 (lambda (s v-sep c)
   (define width
     (+ (* margin 2) client-w))
   (define height
     (+ (* margin 2) client-h))
   (ct-superimpose
    (inset (filled-rectangle width height
                             #:color (make-object color%
                                                  ;; 50 47 47
                                                  237 237 237)
                             #:draw-border? #f)
           (- margin))
    (default-slide-assembler s v-sep c))))

(define (scaled-image filename [height-adjust 0])
  (scale-to-fit (bitmap filename)
                client-w (- client-h height-adjust)))

(define (ascii-art str [size 20])
  (apply vl-append
         (for/list ([line (string-split str "\n")])
           (text line 'modern size))))

(define (gesso pict [bg-color "white"] [inset-size 15])
  (define inset-p
    (inset pict inset-size))
  (cc-superimpose (filled-rectangle (pict-width inset-p)
                                    (pict-height inset-p)
                                    #:color "white"
                                    #:draw-border? #f)
                  inset-p))




(slide
 (bold (titlet "OcapPub, as explained to Friam"))
 (t "Christopher Lemmer Webber")
 (t "https://dustycloud.org/")
 (small (t "Fediverse: https://octodon.social/@cwebber"))
 (small (t "Birdsite: https://twitter.com/dustyweb"))
 (small (t "Original fediverses (email/xmpp): cwebber@dustycloud.org")))

;; AP re-overview

;; Unwanted messages

;; Freedom of speech also means freedom to filter

;; Breadth vs depth?

