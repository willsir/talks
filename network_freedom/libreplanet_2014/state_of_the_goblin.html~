<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>State of the Goblin</title>
<meta name="author" content="(Christopher Allan Webber)"/>

<link rel="stylesheet" href="./reveal.js/css/reveal.min.css"/>
<link rel="stylesheet" href="./reveal.js/css/theme/moon.css" id="theme"/>

<!-- If the query includes 'print-pdf', include the PDF print sheet -->
<script>
    if( window.location.search.match( /print-pdf/gi ) ) {
        var link = document.createElement( 'link' );
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = './reveal.js/css/print/pdf.css';
        document.getElementsByTagName( 'head' )[0].appendChild( link );
    }
</script>
<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
<meta name="description" content="The Road Ahead for Network Freedom">
</head>
<body>
<div class="reveal">
<div class="slides">
<section>
<h1>State of the Goblin</h1>
<h2>Christopher Allan Webber</h2>
<h2><a href="mailto:cwebber@dustycloud.org">cwebber@dustycloud.org</a></h2>
<h2>2014-04-23 Sun</h2></section>

<section id="sec-" >

<h2>About me</h2>
<p>
<i>Hi, I'm Chris Webber!</i>
</p>

<img style="float :right;" src="file:///home/cwebber/devel/dustycloud/static/images/libreplanet_toon_chris.png" />

<ul class="org-ul">
<li class="fragment roll-in">Lead MediaGoblin developer!
</li>
<li class="fragment roll-in">Python developer!
</li>
<li class="fragment roll-in">Free software and free culture activist
<ul class="org-ul">
<li>I've been doing this for a while&#x2026;
</li>
<li>Worked at various FOSS orgs (CC, PCF)
</li>
<li>Now doing MediaGoblin fulltime
</li>
</ul>
</li>
<li class="fragment roll-in">You are not here to hear about me
</li>
</ul>

</section>
<section id="sec-" >

<h2>Framing things</h2>
<img style="border: none; height: 390px;" src="../static/nsa-eagle-mildalterations.png" />

<p>
The present reality of network freedom isn't pretty&#x2026;
</p>

<ul class="org-ul">
<li class="fragment roll-in">Corporate control of the web
</li>
<li class="fragment roll-in">NSA spying
</li>
<li class="fragment roll-in">Freedom for developers, but not for users
</li>
<li class="fragment roll-in">What to do?
</li>
</ul>

</section>
<section>
<section id="sec-" >

<h3>About MediaGoblin</h3>
<p>
MediaGoblin is:
</p>

<ul class="org-ul">
<li class="fragment roll-in">MediaGoblin is a FOSS web media publishing system. (Okay, we've got
a better pitch)
</li>
<li class="fragment roll-in">Released under the AGPL
</li>
<li class="fragment roll-in">A sizeable community: more than 75 contributors, and growing
</li>
<li class="fragment roll-in">A vision-made-reality of what future we want to see
</li>
</ul>
</section>

</section>
<section>
<section id="sec-" >

<h3>MediaGoblin's campaign for network freedom &amp; privacy</h3>
<img style="height: 550px;" src="../static/2014_campaign.png" />

<p>
Running a fundraising campaign with the FSF
</p>

<p>
Let's open it: <a href="http://mediagoblin.org/pages/campaign.html">http://mediagoblin.org/pages/campaign.html</a>
</p>

</section>
<section id="sec-" >

<h4>The video!</h4>
<p>
Going to show the video (best intro we have&#x2026; and frames the rest of this talk)
</p>

<p>
I'm going to kind of improv this audio-wise&#x2026;
</p>
</section>

</section>
<section id="sec-" >

<h2>Messaging</h2>
<p>
One of the most critical components of network / computing freedom success!
</p>

<p>
But wait&#x2026; why?
</p>


</section>
<section>
<section id="sec-" >

<h3>Computing freedom as a human right</h3>
<img style="height: 400px;" src="../static/MannGlas_and_GoogleGlass1_crop.jpg" />

<ul class="org-ul">
<li class="fragment roll-in">Software freedom is treated as a nerd issue, even by most of us
</li>
<li class="fragment roll-in">Maybe an anti-ego self defense (fair enough!)
</li>
<li class="fragment roll-in">But as computing gets closer to directly augmenting thought and
action, computing freedoms no longer a "nerds rights" issue, but a
human rights issue
</li>
</ul>
</section>

</section>
<section>
<section id="sec-" >

<h3>But mostly just nerds understand it :\</h3>
<ul class="org-ul">
<li class="fragment roll-in">Outside of hackers and academics, how many people understand what
FOSS really means, as an ethical issue?
</li>
<li class="fragment roll-in">Network non-freedom has a network effect
</li>
<li class="fragment roll-in">Freedom for the technically-skilled few, or freedom for society?
</li>
<li class="fragment roll-in">Surely we're in the broader social movement camp!
</li>
<li class="fragment roll-in">Smugness is not the answer
</li>
</ul>
</section>

</section>
<section>
<section id="sec-" >

<h3>Can we make better resources to improve awareness?</h3>
<img src="../../mediagoblin/static/censor_scan.png" alt="censor_scan.png" />

<ul class="org-ul">
<li class="fragment roll-in">"Teachable moments" abound
</li>
<li class="fragment roll-in">What if we created materials that explain core principles of free software?
</li>
<li class="fragment roll-in">MediaGoblin campaign video partly an attempt to prove this possible
</li>
</ul>
</section>

</section>
<section>
<section id="sec-" >

<h3>Neither tech nor messaging independently enough</h3>
<ul class="org-ul">
<li class="fragment roll-in">Laws are not enough
<ul class="org-ul">
<li>Seems unlikely corporate surveillance/censorship will be made illegal
</li>
<li>Much of it probably illegal, and happened anyway!
</li>
<li>But policy reform <i>is</i> critical, certainly
</li>
</ul>
</li>
<li class="fragment roll-in">Messaging is not enough
<ul class="org-ul">
<li>If people know there's a problem but have no way to change their
habits, things will not change
</li>
<li>It's a metric for how bad things are that most internet activist
campaigns point back to FB/T/G+
</li>
</ul>
</li>
<li class="fragment roll-in">Tech is not enough
<ul class="org-ul">
<li>If we build it and nobody knows it or how to use it,
what's the point?
</li>
<li>I iterated on this already enough
</li>
</ul>
</li>
</ul>

</section>
<section id="sec-" >

<h4>Let's work together</h4>
<p>
tech + policy + messaging = &lt;3
</p>

<p>
There's never been a better time to bring us together&#x2026; let's take
advantage of it!
</p>
</section>

</section>
<section id="sec-" >

<h2>Licensing and community</h2>
<p>
Lots of stress about the adoption of copyleft vs permissive
licenses&#x2026;
</p>

<img src="../static/dont_panic_terminal_crop.png" alt="dont_panic_terminal_crop.png" />

<p>
but maybe this is a symptom of something else?
</p>

</section>
<section>
<section id="sec-" >

<h3>The mid-2000s-&gt;today shift</h3>
<img style="border: none;" src="../static/open_source_almost_everything.png" />

<ul class="org-ul">
<li class="fragment roll-in">Rise of the "web 2.0 world"
</li>
<li class="fragment roll-in">The release of the "mactel" leads to large apple switchover
</li>
<li class="fragment roll-in">Rise in anti-copyleft sentiments
</li>
<li class="fragment roll-in">"Release everything but your secret sauce"
</li>
<li class="fragment roll-in">Anti-copyleft attitude is <i>not</i> mostly coming out of a principled
anti-copyright policy, but a "we want to proprietize"
strategy (tricky!)
</li>
</ul>

<p class="fragment roll-in">
Most devs make the license choices their role models do&#x2026;
So, who are the role models of today's webdev hackers?
</p>
</section>

</section>
<section>
<section id="sec-" >

<h3>Are we in a copyleft crisis?  Or an ethical crisis?</h3>
<img src="../../mediagoblin/static/agplv3-whitebg.png" alt="agplv3-whitebg.png" />

<p>
Libraries are always more condusive towards permissive licenses
</p>

<p>
That's okay!  We're doing <b>just fine</b> infrastructure-wise
</p>

<p>
We're really struggling not on the developer-freedom-tooling side, but
the user-freedom-tooling side&#x2026;
</p>
</section>

</section>
<section>
<section id="sec-" >

<h3>People still seem to choose copyleft for web <b>applications</b></h3>
<p>
To name a few&#x2026;
</p>

<ul class="org-ul">
<li class="fragment roll-in">Wordpress (GPL)
</li>
<li class="fragment roll-in">MediaWiki (GPL)
</li>
<li class="fragment roll-in">CiviCRM (AGPL)
</li>
<li class="fragment roll-in">StatusNet / GNU Social (AGPL) # &lt;- pump.io licensing switch was partly
for "become infrastructure" reasons
</li>
<li class="fragment roll-in">Pagekite (AGPL)
</li>
<li class="fragment roll-in">Mailpile (AGPL)
</li>
<li class="fragment roll-in">Diaspora (AGPL)
</li>
<li class="fragment roll-in">MediaGoblin (AGPL) ;)
</li>
<li class="fragment roll-in">Gitorious (AGPL)
</li>
</ul>
</section>

</section>
<section>
<section id="sec-" >

<h3>In other words</h3>
<p>
We're struggling with getting polished user-facing software (and in
the hands of users)
</p>

<p>
But when it happens, copyleft seems a common choice
</p>
</section>

</section>
<section>
<section id="sec-" >

<h3>Freedom-aware communities are the antidote</h3>
<ul class="org-ul">
<li class="fragment roll-in">Build the developer-focused tooling of tomorrow, but with
pro-freedom communities
<ul class="org-ul">
<li>Lots of opportunities&#x2026; but that's another talk&#x2026;
</li>
<li>Permissive licensing is probably fine here!
</li>
</ul>
</li>
<li class="fragment roll-in">Build the user-oriented networked appliations of tomorrow
(like MediaGoblin? ;))
<ul class="org-ul">
<li>People will probably choose copyleft licenses for these!
</li>
<li>Free talking point: "I copyleft what you proprietize"
</li>
<li>There aren't enough polished examples though
</li>
</ul>
</li>
</ul>
</section>

</section>
<section id="sec-" >

<h2>Deployment / adoption</h2>
<p>
User freedom oriented software requires our software is used by
users :)
</p>

<p>
This means quality software, but also deployable software
</p>

<p>
We're struggling here&#x2026;
</p>

</section>
<section>
<section id="sec-" >

<h3>Deployment is hard</h3>
<ul class="org-ul">
<li class="fragment roll-in">Often not packaged, but even if so
</li>
<li class="fragment roll-in">Trend towards language-specific packaging and environments (see
Virtualenv)
</li>
<li class="fragment roll-in">Editing application config files, getting about 5 separate
application configs in sync
</li>
</ul>
</section>

</section>
<section>
<section id="sec-" >

<h3>Deployment of free network services: challenging :\</h3>
<ul class="org-ul">
<li class="fragment roll-in">Most modern web applications
<ul class="org-ul">
<li>Challenging to deploy if not a $LANGUAGE developer
</li>
<li>But we have to engage it
</li>
</ul>
</li>
<li class="fragment roll-in">Users don't tend to see these issues because companies abstract
deployment away.  Network freedom doesn't have that luxury!
</li>
</ul>
</section>

</section>
<section>
<section id="sec-" >

<h3>What about PHP?</h3>
<ul class="org-ul">
<li class="fragment roll-in">"Sort of" easy, but only if you're using shared hosting
</li>
<li class="fragment roll-in">The things that make it "easy" make it easily exploited
</li>
<li class="fragment roll-in">Shared hosting is dying (good riddance)
</li>
<li class="fragment roll-in">PHP is living in the past, dude
</li>
</ul>
</section>

</section>
<section>
<section id="sec-" >

<h3>Not just web services!  Email is easy, right?</h3>
<ul class="org-ul">
<li class="fragment roll-in">Email is federated, has been around a long time
</li>
<li class="fragment roll-in">"How hard could it be?  Surely a solved problem, I just install a package?"
</li>
<li class="fragment roll-in">"Tell me about your setup, Chris"
</li>
</ul>
</section>

</section>
<section>
<section id="sec-" >

<h3>We need user-centric config/deployment management</h3>
<img src="../static/deployment_logos.png" alt="deployment_logos.png" />

<ul class="org-ul">
<li class="fragment roll-in">We need some layers above the packaging world
</li>
<li class="fragment roll-in">Cool things happening in the world of deployment, but they're mostly
sysadmin/corporate oriented!  (Salt, Puppet, Docker)
</li>
<li class="fragment roll-in">We need to make config/deployment management for people who will never
touch a config file
</li>
<li class="fragment roll-in">PaaS?  OpenShift???
</li>
</ul>
</section>

</section>
<section>
<section id="sec-" >

<h3>"A layer above apt"?</h3>
<p>
What I'd like to see:
</p>

<ul class="org-ul">
<li class="fragment roll-in">A configuration recipe system designed for users, not sysadmins
</li>
<li class="fragment roll-in">"A layer above apt"
</li>
<li class="fragment roll-in">Super minimalist, hopefully a web interface, but maybe something
not far from SaltStack?
</li>
<li class="fragment roll-in">Generic: not just for VMs, not just for plug computers
</li>
<li class="fragment roll-in">GNUToo: "Maybe look at Luci" (I haven't yet)
</li>
</ul>
</section>

</section>
<section>
<section id="sec-" >

<h3>The silver lining</h3>
<img src="../static/install_fest.jpg" alt="install_fest.jpg" />

<ul class="org-ul">
<li class="fragment roll-in">The free software desktop used to be hard to run and maintain also
</li>
<li class="fragment roll-in">We need to encourage exploring this frontier until it gets easier
</li>
<li class="fragment roll-in">When things are hard, that's an opportunity to learn cool things
</li>
<li class="fragment roll-in">Bring back the install fest!
</li>
</ul>
</section>

</section>
<section id="sec-" >

<h2>Federation</h2>
<img src="../static/purple_federation.png" alt="purple_federation.png" />

<p>
Not just for Star Trek!
</p>

</section>
<section>
<section id="sec-" >

<h3>The current state of federation</h3>
<img src="../static/ostatus_to_pump.png" alt="ostatus_to_pump.png" />

<p class="fragment roll-in">
<i>(Note: StatusNet != OStatus; Pump.IO != Pump API)</i>
</p>

<p class="fragment roll-in">
Why the switch?
</p>

<ul class="org-ul">
<li class="fragment roll-in">Cleaner
</li>
<li class="fragment roll-in">Easy to understand
</li>
<li class="fragment roll-in">Privacy
</li>
<li class="fragment roll-in">Switch from StatusNet (PHP!) to Node an opportunity to reapproach things
</li>
</ul>
</section>

</section>
<section>
<section id="sec-" >

<h3>Federation, MediaGoblin, and PyPump</h3>
<img src="file:~/gfx-proj/mediagoblin_pitch2/videos/pngs/pypump/00202.png" alt="00202.png" />
</section>

</section>
<section id="sec-" >

<h2>Cohesiveness</h2>
<img src="../static/jpope_lotsa_applications_bigger.png" alt="jpope_lotsa_applications_bigger.png" />

<p>
So, federation is cohesion between sites, but what about within a site
itself?
</p>

<ul class="org-ul">
<li class="fragment roll-in">Multiple applications hard to theme
<ul class="org-ul">
<li>Different templating languages
</li>
<li>Different layout decisions and patterns
</li>
</ul>
</li>
<li class="fragment roll-in">Differeing and incompatible authentication systems
</li>
<li class="fragment roll-in">Other inter-application inconsistencies 
</li>
</ul>

</section>
<section>
<section id="sec-" >

<h3>Do we need a "desktop suite" of web applications?</h3>
<object type="image/svg+xml" data="../static/GnomeLogoVertical.svg" >
Sorry, your browser does not support SVG.</object>

<p>
???
</p>
</section>

</section>
<section id="sec-" >

<h2>Accomplishments of the last year</h2>
<img style="height: 400px;" src="../static/goblin_force_bottom.jpg" />

<ul class="org-ul">
<li class="fragment roll-in">Five <b>major</b> releases
</li>
<li class="fragment roll-in">Six successful Outreach Program for Women and Summer of Code internships
</li>
<li class="fragment roll-in">One grant-funded project for MediaGoblin in academic institutions
</li>
</ul>

<p class="fragment roll-in">
<i>What a deal!</i>
</p>

</section>
<section id="sec-" >

<h2>So where is MediaGoblin now?</h2>
<p>
We're pretty close&#x2026;
</p>

<ul class="org-ul">
<li class="fragment roll-in">image/video/audio hosting already hosts pretty well
</li>
<li class="fragment roll-in">Already very extensible, great plugin/theming system
</li>
<li class="fragment roll-in">Things mostly work&#x2026; we're almost at 1.0!  Except&#x2026;
<ul class="org-ul">
<li>We need federation features
</li>
<li>We want privacy features
</li>
<li>Various other bits of polish
</li>
</ul>
</li>
<li class="fragment roll-in">We're so close&#x2026;!
</li>
</ul>

</section>
<section>
<section id="sec-" >

<h3>But is that it?</h3>
<img src="file:~/devel/mediagoblin-website/static/blog_images/think_of_the_kittens.png" alt="think_of_the_kittens.png" />

<p>
Are we calling it quits and leaving the kittens out in the cold?
</p>

<p class="fragment roll-in">
<i>No!</i>
</p>
</section>

</section>
<section id="sec-" >

<h2>So, back to this campaign</h2>
<img style="height: 300px;" src="file:///home/cwebber/devel/mediagoblin-website/static/images/campaign/milestone_characters/unlock_characters.png" />

<p>
Super easy to find!  Let's go there right now:
</p>

<p>
<a href="http://mediagoblin.org/pages/campaign.html">http://mediagoblin.org/pages/campaign.html</a>
</p>

<p>
Or from the homepage even!
</p>

</section>
<section>
<section id="sec-" >

<h3>A critical opportunity</h3>
<img style="height: 500px;" src="file:///home/cwebber/gfx-proj/mediagoblin_pitch2/renders/shots/finale/0406.png" />

<ul class="org-ul">
<li>There's never been more awareness of why this stuff is needed
</li>
<li>We're definitely at a crossroads
</li>
<li>We have the people, the community&#x2026; we can do this!
</li>
<li>But we need <b>YOU</b>!
</li>
</ul>
</section>

</section>
<section id="sec-" >

<h2>Credits (p1)</h2>
<p>
Mann Glass and Google Glass picture: CC BY-SA 3.0,
</p>
<span style="text-size: 5"><a href="http://en.wikipedia.org/wiki/File:MannGlas_and_GoogleGlass1_crop.jpg">http://en.wikipedia.org/wiki/File:MannGlas_and_GoogleGlass1_crop.jpg</a></span>

<hr />

<p>
Pump.io image parody, uses a bunch of Evan Prodromou's heads, plus image from CC BY 2.0 image:
</p>
<span style="text-size: 5"><a href="http://www.flickr.com/photos/makelessnoise/240072395/">http://www.flickr.com/photos/makelessnoise/240072395/</a></span>

<hr />

<p>
NSA graphix, courtesy EFF, CC BY 3.0 US: <a href="https://www.eff.org/pages/eff-nsa-graphics">https://www.eff.org/pages/eff-nsa-graphics</a>
</p>

<hr />

<p>
All of GNOME, OpenShift, etc&#x2026; logos are belong to them, we have no
chance to survive, observe their trademark policies
</p>

</section>
<section id="sec-" >

<h2>Credits (p2)</h2>
<p>
List of icons from <a href="http://media.jpope.org/">http://media.jpope.org/</a>
</p>

<hr />

<p>
Install fest pic, CC BY 3.0 US:
</p>
<span style="text-size: 5"><a href="http://commons.wikimedia.org/wiki/File:LIFS2011.jpg">http://commons.wikimedia.org/wiki/File:LIFS2011.jpg</a></span>

<hr />

<p>
Music from second campaign video, "A Dark Hero", CC BY-SA 3.0
<a href="http://www.jamendo.com/en/track/1010817/a-dark-hero">http://www.jamendo.com/en/track/1010817/a-dark-hero</a>
</p>

<hr />

<p>
Quick thanks to Bassam Kurdali, Fateh Slavitskaya, Deb Nicholson for
help on the video plus a million things
</p>

<hr />

<img style="border: none;" src="../static/cc_by-sa.png" />

<p>
This talk CC BY-SA 4.0 International
</p>

</section>
<section id="sec-" >

<h2>Please help :)</h2>
<img style="height: 300px;" src="../../mediagoblin/static/goblin_force.png" />

<ul class="org-ul">
<li class="fragment roll-in">Join our community! Use &amp; contribute: <a href="http://mediagoblin.org/">http://mediagoblin.org/</a>
</li>
<li class="fragment roll-in">Please donate in the campaign (everything helps, seriously)
</li>
<li class="fragment roll-in">Please spread the word
</li>
</ul>

<p class="fragment roll-in">
Thank you!  Any questions?
</p>
</section>
</div>
</div>

<script src="./reveal.js/lib/js/head.min.js"></script>
<script src="./reveal.js/js/reveal.min.js"></script>

<script>
// Full list of configuration options available here:
// https://github.com/hakimel/reveal.js#configuration
Reveal.initialize({

controls: true,
progress: true,
history: false,
center: true,
slideNumber: true,
rollingLinks: true,
keyboard: true,
overview: true,
width: 1200,
height: 800,
margin: 0.10,
minScale: 0.50,
maxScale: 2.50,

theme: Reveal.getQueryHash().theme, // available themes are in /css/theme
transition: Reveal.getQueryHash().transition || 'cube', // default/cube/page/concave/zoom/linear/fade/none
transitionSpeed: 'default',

// Optional libraries used to extend on reveal.js
dependencies: [
 { src: './reveal.js/lib/js/classList.js', condition: function() { return !document.body.classList; } },
 { src: './reveal.js/plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
 { src: './reveal.js/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
 { src: './reveal.js/plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
 { src: './reveal.js/plugin/zoom-js/zoom.js', async: true, condition: function() { return !!document.body.classList; } },
 { src: './reveal.js/plugin/notes/notes.js', async: true, condition: function() { return !!document.body.classList; } }
]
});
</script>
</body>
</html>
