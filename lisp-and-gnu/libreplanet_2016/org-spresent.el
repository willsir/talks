;; -*- lexical-binding: t -*-

;;; org-spresent --- Simple org-mode presentation
;;; Copyright © 2017 Christopher Allan Webber <cwebber@dustycloud.org>
;;;
;;; This file is part of org-spresent.
;;;
;;; org-spresent is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; org-spresent is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with org-spresent.  If not, see <http://www.gnu.org/licenses/>.


(defun org-spresent-next-sp ()
  (save-excursion
    (widen)
    ;; Keep searching until we find a headline that's tagged with sp
    ;; or we get until the end of the buffer
    (outline-next-heading)
    (while (not (or (= (point) (point-max))
                    (org-spresent-tagged-with-sp-p)))
      (outline-next-heading))
    ;; return position
    (point)))

(defun org-spresent-prev-sp ()
  (save-excursion
    (widen)
    ;; Keep searching until we find a headline that's tagged with sp
    ;; or we get until the start of the buffer
    (outline-previous-heading)
    (while (not (or (= (point) (point-min))
                    (org-spresent-tagged-with-sp-p)))
      (outline-previous-heading))
    ;; return position
    (point)))

(defun org-spresent-next-slide ()
  (interactive)
  (let ((next-sp (org-spresent-next-sp)))
    (if (= next-sp (point-max))
        (message "End of presentation!")
      (progn
        (widen)
        (goto-char next-sp)
        (org-overview)
        (org-show-context)
        (org-show-siblings)
        (org-cycle)
        (narrow-to-region (point) (- (org-spresent-next-sp) 1))))))

(defun org-spresent-tagged-with-sp-p ()
  (member "sp" (org-get-tags)))


;; org-cycle-internal-local
