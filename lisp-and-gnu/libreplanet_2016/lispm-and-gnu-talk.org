#+TITLE: The Lisp Machine and GNU

Hello Libreplanet!

:     *      .-.                      .
:           (   )      .     *               *
:     _      '-'    ,              ,    ,
:    (_)  _ /   \.--.\         .   \\___//    .
:     _ >(_)    "    ;"           _ ; o o\ _
:    (_)       (    / )          / '------' \
:               ;._/."    . O () "._o_o_o_o."
:          *    \.'    .           .           *

* GNU                                                                    :sp:
** What's GNU?

:  /\            /\
: / /    ___ __  \ \
: \ \__.'   '  './ /
:  \___.----'---___/
:     /    @    @_
:    /  )     _   '
:   ;_-'    _(,\__,)
:       \   \'uuuu'
:        "", '---'
:          """,--,
:            '"\\\

 - GNU's Not Unix!
 - A free operating system
 - A vision

*** Being "not unix" is the coolest!                                     :sp:

Unix didn't invent most of these, but still nice...

 - C as "lingua franca" means portable ("high level assembly")
 - Heirarchical filesystem!
 - Command line power!
 - "Everything is a file"
 - Plaintext is first class

*** Why not not unix?                                                    :sp:

Isn't being (a) not unix the best???

**** Manual memory management in C                                       :sp:

After all, what's GNU?
:     .-----------'||
:     |     .------'|
:     |     |   .---'    
:     V     V   V
***** GNU's Not Unix
:     ||'--------.
:     |'-----.   |
:     '.     |   |
:      V     V   V
****** GNU's Not Unix
:      ||'--------.
:      |'-----.   |
:      '.     |   |
:       V     V   V
******* GNU's Not Unix
:       ||'--------.
:       |'-----.   |
:       '.     |   |
:        V     V   V
******** GNU's Not Unix
:
:       **********************************
:       * !!!!!!! STACK OVERFLOW !!!!!!! *
:       **********************************
:
:      /\_/\          '-___-'          /\_/\
:     / -.- \         / 0.0 \         / 0.0 \
:    == _|_ == _     == _|_ == _     == _X_ == _
:     /     \ / \     /     \ / \     /     \ / \
:    | || || | /\|   | || || | /\|   | || || | /\|
:   |_|_|_|_|_|     |_|_|_|_|_|     |_|_|_|_|_|
: 
:    SEE NO CVE      HEAR NO CVE    SPEAK NO CVE

**** Stringly typed systems                                              :sp:

Plaintext is great!
But a lack of structure leads to...

[[file:../static/exploits_of_a_mom.png]]

 - Injection attacks ("Why are we speaking in sentences?")
 - Shell filename splitting/iteration
 - Anything involving "xargs"
 - You can't compose string-based regexps, etc
   (By contrast, you can compose [[file:../static/ledger.scm][irregex]]!)
   
**** Etc, but this is not a (not-)unix hating talk                       :sp:

[[file:../static/uhh-title.gif]]

There's a whole (curmudgeonly) book for that!

Being a (Not-)Unix has nice properties,
but "(Not) Unix" is hardly the only game in town.

** Other ideas: the lisp machine                                         :sp:

[[file:../static/181px-LISP_machine.jpg]]

*** Cool features

 - Dynamic, like interpreted languages, but compiled to native code
 - Garbage collection... even in hardware! (eventually)
 - Inspect, explore, modify *any* part of the operating system
 - Built in windowing framework!  Mouse driven contexts!
 - REPL with more than just text!

   [[file:../static/mcclim_screenshot.png]]

 - Came with super cool keyboards

   [[file:../static/Space-cadet.jpg]]

*** Not claiming lisp machines are prefect!                              :sp:

 - Single-user machines
 - No process isolation
 - Required special hardware
 - Died in late 1980s / early 1990s

:               *                     *
:  *                     *                  _.     *
:       *                     *            <  '.
:              *                            )ual     *
:                                    *     <_.'   *
:     *      *       .------------.
:                  .'              '.                *
:        *         |      THE       |   *
:                  |  LISP MACHINE  |       *
:             *    |                |            *
:    *             |                |
:                  |   BORN: ~1974  |
:              .^. |   GC'D: ~1987- |
:   _  .+.  _  |~| |          1990  |  .+. .-.  _  .-.
:  | | |~| |=| | | |                |  |=| |~| | | | |
: ``'`'`''``'`'`'`'``'``'`'`''``''`'`'``'`''``''``'`'`'

** Wait, why wasn't GNU a lisp machine???                                :sp:

I heard rms working on those right before GNU???

Good question!  Let's dig deeper...

* What's lisp                                                            :sp:
** Why lisp?                                                             :sp:

:    \'.
:     \ '.                      .-****-.
:      \  \                    *       _*
:      _L__L_           _     * EVAL .'  *
:      //~ ~           / /    *.___.'    *
:      G   >           //     '.   APPLY.'
:      / \\\\-------.,//        *.____.*
:     / ' \\\______ |;3
:    |   \ \\      \//

(Lisp Is Syntactically Pure)

 - Conceptually lean but powerful
   - based off the lambda calculus
   - s-expressions are "homoiconic"... "code that writes code"
 - "living" development environments
 - interpreted?  compiled?  many lisps are both!
 - Lisp seems to have everything
 - And if it doesn't, you can write it!

** A mini tutorial

To the REPL!

** Language exploration with lisp                                        :sp:

No need to beg for features from language designers...
YOU can be a language designer!

*** Macros make it easy

Want "yield from" but Python 2 doesn't have it?  No problem in a lisp:

#+BEGIN_SRC lisp
  (defmacro yield-from [expr]
    `(do (import types)
         (setv ~g!iter (iter ~expr))
         (setv ~g!return nil)
         (setv ~g!message nil)
         (while true
           (try (if (isinstance ~g!iter types.GeneratorType)
                  (setv ~g!message (yield (.send ~g!iter ~g!message)))
                  (setv ~g!message (yield (next ~g!iter))))
           (catch [~g!e StopIteration]
             (do (setv ~g!return (if (hasattr ~g!e "value")
                                     (. ~g!e value)
                                     nil))
               (break)))))
           ~g!return))
#+END_SRC

From https://github.com/hylang/hy/pull/686

*** Lisp in lisp (or: metacircular evaluation)

Once you have a lisp, it's easy to get another lisp

Variants!  Logic programming, etc...

So, lispers can easily explore language ideas...

** Lispers: the Cassandras of CS?                                        :sp:

[[file:../static/cassandra.png]]

** A language of (too?) many dialects                                    :sp:

#+BEGIN_QUOTE
  Scott Fahlman said, "the MacLisp community is not in a state of
  chaos. It consists of four well defined groups going in four
  well-defined directions." [[Steele, 1982] The Evolution of Lisp]
#+END_QUOTE

*** Classic heavy hitters
**** Common Lisp

[[file:../static/lisplogo_alien_256.png]]

 - The Great Unifier of the 1980s
 - Very portable across implementations
 - Lots of features (CLOS, wide variety of procedures)

Implementations: SBCL, clisp...

**** Scheme

[[file:../static/wizard.jpg]]

 - Cleanest foundation
 - Easy to implement
 - "the world's most unportable programming language" -- Scheme
   Steering Committee
 - My favorite

Implementations: Guile, MIT Scheme, Chicken... and so many more

**** Emacs Lisp

[[file:../static/emacs.png]]

 - GNU Emacs' language
 - Kludgy foundation, but a workhorse
 - Comes with the kitchen sink, and then some

*** Modern lisps!

**** Clojure

[[file:../static/240px-Clojure_logo.svg.png]]

 - Built on the JVM
 - Very functional
 - Lisp-2 (if you care)
 - Great community
 - Eclispe Public License means GPL incompatible :(

**** Hy?!?!?!

[[file:../static/198px-Hy_Cuddles.png]]

 - Lisp -> Python AST
 - Definitely not a joke

:   LET'S CUDDLEFISH
:              ______
:       _.----'#'  # '
:     ,'  #'    ,#  ;
:    ('   (w)  _,-'_/
:   /// / /'.____.'
:   \|\||/

*** Historic

 - InterLisp
 - MacLisp
 - Muddle / MDL
 - ... too many ...!

** Lisp is beautiful                                                     :sp:
     
learning to love the parentheses

 - [[file:../static/grep.scm][A Guix package written in s-expressions!]]
 - [[file:../static/grep.w][A Guix package written in Wisp!]]

** Lisp timeline and friends                                             :sp:

:               ,
:        ______|#|______
:      _[_______________]_
:    [|__POLICE_"""_BOX___|]
:     ||  _____ | _____  ||
:     || | | | ||| | | | ||
:     || |-|-|-|||-|-|-| ||
:     || |_|_|_|||_|_|_| ||
:     || ,-----,|,-----, ||
:     || |,---,|||     | ||
:     || ||,.,|||#     | ||
:     || '-----'|'-----' ||
:     || ,-----,|O-----, ||
:     || |     |||     | ||
:     || |     |||     | ||
:     || '-----'|'-----' ||
:     || ,-----,|,-----, ||
:     || |     |||     | ||
:     || |     |||     | ||
:     ||_'-----';'-----'_||
:    [_____________________]

(I'm leaving out a lot, due to time...)

*** Lisp pre-history

:            ..   ..
:             \.-./
:       \     (o o)    \
:       /\    /"""\    /\
:            ''   ''
:   BEWARE OF FALLING LAMBDAS!

 - 1956-1958: AI summit; McCarthy inspired to make "algebraic lisp
   processing language"

 - 1958-1960: First Lisp developed
   - "Recursive functions of symbolic expressions and their
      computation by machine, part I" by McCarthy
      Includes definition of lisp in lisp!
   - s-expressions and m-expressions... but sexps stick!
   - Steve Russel adds interpreter based on definition in paper...
     and lisp takes off
   - Phyllis Fox writes first documentation

#+BEGIN_QUOTE
  Nobody in that group ever wrote down anything. McCarthy was furious
  that they didn’t document the code, but he wouldn’t do it,
  either. So I learned enough LISP that I could write it and ask them
  questions and write some more.
    -- [[http://history.siam.org/oralhistories/fox.htm][Phyllis Fox]]
#+END_QUOTE

Tons of new ideas, eg garbage collection
       
Context of computing time: Lisp develops *if / else* statements!

*** 1960s-1970s: Hacker culture co-evolves with lisp

 - 1960s-1970s: lisp and hacker culture co-evolve at MIT AI lab
   (MUCH of hacker culture today develops from this period...)
 - Maclisp on ITS
   - Explosion of dialects!
   - but lisp is slow!
 - rms finds his community in the hacker labs
 - Scheme, others written (Lambda Papers!)
 - 1974­1978: MIT lisp machines developed (CONS and CADR)
 - rms becomes maintainer of lisp machine lisp

*** Early 1980s: The LMI and Symbolics split off

**** Lisp machine companies start

 - Two companies emerge, both developing from CADR's designs
   - Richard Greenblatt founds Lisp Machines Incorporated
     - "No outside investors"
     - Aims to hire hackers part-time from lab
   - Russell Noftsker founds Symbolics
     - Takes venture capital money
     - Hires most of the hackers

**** Fracturing begins

 - MIT signs agreement that says that Symbolics / LMI must share
   changes with MIT, but didn't permit redistribution outside of the
   lab

 - Due to hacker spirit, for first year, sharing happens anyway
 - Symbolics says... "Hey, we're making most of the advancements"
 - So along comes to INFO-LISPM mailing list:
   
   #+BEGIN_QUOTE
     This software is provided to MIT under the terms of the Lisp
     System License Agreement between Symbolics and MIT. MIT's license
     to use this software is non-transferable. This means that the
     world loads, microloads, sources, etc. provided by Symbolics may
     not be distributed outside of MIT without the written permission
     of Symbolics.
       -- http://ml.cddddr.org/info-lispm/msg00267.html
   #+END_QUOTE
   
   tl;dr: Symbolics can use your software, and you can use ours in the
     lab, but sharing is forbidden!
   
 - Note that LMI signed the same agreement!
   (MIT's lawyers drafted the text, as rms says, making MIT's work
   proprietary)
   But rms blames Symbolics for killing the lab by violating hacker
   spirit (and

**** Where to from here?

 - RMS keeps working on MIT's Lisp Machine Lisp to punish Symbolics
   for two years...
 - RMS publishes GNU announcement on September 27, 1983

*** Mid to late eighties: Lisp takes off outside the lab

**** Common lisp consolidation

**** Scheme standardization

[[file:../static/dr_sussman.png]]

**** Symbolics and other lisp machine companies

Unfortunately, an era of incredible but proprietary developments

[[file:../static/symbolics-demo-screenshot-1.png]]

Note that UNIX at this time was also highly fractured and
incompatible.  GNU saved the Not Unix!

*** ~1990: The AI winter gets frosty

AI promise was oversold.  Over exuberant venture capital pulls out.

Symbolics doesn't free its code... if it did, it would probably still
be alive today.

[[file:../static/symbolics-demo-guy-1.png]]

THE FUTURE THAT COULD HAVE BEEN

* Lisp and GNU                                                           :sp:

:  (  ( (   /\            /\  )
:          / /    ___ __  \ \
:          \ \__.'   '  './ /
:       ((  \___.----'---__/  )      ))
:              /    @    @_
:       ((    /  )     _   '        )
:   ((       ;_-'    _(,\__,)
:                \   \'uuuu'     )
:                 "", '---'        )
:         ((        """,--,
:                     '"\\\   )))

** So, why Unix?  Why not a lisp machine?

rms told me: main decision in GNU was whether to implement as a lisp
machine or (Not) Unix

 - lisp machines required special hardware to be fast and safe
   (type checking was done in hardware!)
 - GNU was meant to run on conventional hardware
   (and previous loved hacker systems, various lispm and ITS, were
   not portable...)
 - Also, what lispm design would be a "research effort", and could
   be wrong

#+BEGIN_QUOTE
  Why not aim for a new, more advanced system, such as a Lisp Machine?
  Mainly because that is still more of a research effort; there is a
  sizeable chance that the wrong choices will be made and the system
  will turn out not very good.  In addition, such systems are often tied
  to special hardware.  Being tied to one manufacturer's machine would
  make it hard to remain independent of that manufacturer and get broad
  community support.
#+END_QUOTE

https://www.gnu.org/bulletins/bull1.txt

** C and Lisp!  The official languages of GNU!

 - So!  The decision made to make lisp a user program on top of Unix.
 - Would be portable (because C)
 - Could also delay making the decisions on lisp or change them later
 - Lisp planned as part of beginning (from GNU initial announcement):

#+BEGIN_QUOTE
  GNU will be able to run Unix programs, but will not be identical to
  Unix. [...] perhaps eventually a Lisp-based window system through
  which several Lisp programs and ordinary Unix programs can share a
  screen. Both C and Lisp will be available as system programming
  languages.
#+END_QUOTE

** Emacs                                                                 :sp:

*** What is an emacs?

 - An editor
 - C core, extensible in lisp
 - To configure it is to extend it!
 - Emacs can do ANYTHING!
   - Tetris?
   - Email?
   - [[file:~/devel/wireworld-el/wireworld-demo.ww][Wireworld?]]
   - Also edit source code

**** History of emacsen

 - GNU Emacs is not the first emacs!
 - Original ITS Emacs
   - primarily developed / maintained by rms
   - Uses terrible "TECO" language
   - First full screen editor!
   - Influenced rms's decision to require sharing changes
 - GNU Emacs' "C with lisp core" influenced by:
   - Multics' emacs, which was implemented in pure lisp
     (with some extensions to Multics)
   - An emacs implementation, name of which rms does not remember (???)
     which also had fast core with userspace lisp

GNU Emacs is GNU's first program, but also massively alive today

*** GNU Emacs: the poor man's lisp machine?

[[file:../static/lispm-like-emacs.png]]

[[file:../static/emacs-like-lispm.png]]

*** Maybe not exactly

[[file:../static/rainer-joswig.jpg]]

#+BEGIN_QUOTE
  <Rainer Joswig> does that make sense? 'Emacs' is a family of text
    editors. 'Lisp Machine' is a family of computers.
  <Rainer Joswig> but Emacs is not an OS. It's just a layer to write
    text-oriented applications on top of a real OS.
  <Rainer Joswig> Check the video link I posted. Does GNU Emacs look
    like that in any way? On a Lispm Emacs is just one possible app.
#+END_QUOTE

** Guile and the Emacs Thesis                                            :sp:

[[file:../static/guile-extendability.jpg]]

A hybrid vision!

Though... in practice this hasn't happened much?

** GNU and Lisp now                                                      :sp:

 - Guile 2.2!
   [[file:../../guix/chicagolug_2015/static/guile-logo.png]]

 - Guix
   [[file:../../guix/static/guixsd_logo.png]]
   
 - Shameless plug: 8sync!
   [[file:../../guix/static/8sync.png]]

 - What else could happen?

** Some lessons and reflections                                          :sp:

 - Lisp is beautiful and not scary!  Just different
 - You can, and should, learn lisp!
 - Lisp is part of our heritage
 - Hopefully, part of our future
 - Join us!

[[file:../static/xkcd-lisp_cycles.png]]

[[file:../static/xkcd-lisp.jpg]]

* Thanks                                                                 :sp:
** Image Credits

 - Cassandra image macro, based off of Cassandra by Evelyn De Morgan
   (public domain):
   https://en.wikipedia.org/wiki/Cassandra#/media/File:Cassandra1.jpeg
 - Lisp alien (public domain by Conrad Barski):
   http://www.lisperati.com/logo.html
 - SICP image from video lectures (CC BY-SA 3.0):
   https://groups.csail.mit.edu/mac/classes/6.001/abelson-sussman-lectures/
 - Clojure logo (nonfree, fair use!)
   https://en.wikipedia.org/wiki/File:Clojure_logo.svg
 - "Exploits of a Mom" / Little Bobby Tables
   and "Lisp" and "Lisp Cycles" comics by xkcd
   (CC BY-NC 2.5)
   https://www.xkcd.com/327/
   https://xkcd.com/297/
   https://www.xkcd.com/224/
 - Space Cadet image by Dave Fischer
   (CC BY-SA 3.0)
   https://commons.wikimedia.org/wiki/File:Space-cadet.jpg
 - Hy cuddlefish logo by Karen Rustad, CC0
 - Symbolics 80s man pictures and video editing demo from Symbolics
   Demo video here:
   https://www.youtube.com/watch?v=8RSQ6gATnQU
 - Lisp machine picture by Jszigetvari
   https://commons.wikimedia.org/wiki/File:LISP_machine.jpg
 - CADR zmacs screenshot from
   http://www.unlambda.com/cadr/
 - Guile logo, Guix logo, Guile person drawing by sirgazil
 - Rainer Joswig image from
   https://twitter.com/rainerjoswig
 - Ascii art... by myself


** Historical Documents

 - Evolution of Lisp paper/book
 - "My Lisp Experiences and the Development of GNU Emacs"
   by Richard Stallman
   https://www.gnu.org/gnu/rms-lisp.html
 - The book "Hackers" by Steven Levy (has some problems, particularly
   regarding its gendered portrayals in the lab, but apparently gets
   lisp machine split mostly right)
 - Old lisp mailing list gems
   https://dustycloud.org/blog/old-lisp-mailing-list-gems/
 - GNU announcement and manifestio
   https://www.gnu.org/gnu/initial-announcement.html
   https://www.gnu.org/gnu/manifesto.html

* BONUS CONTENT!                                                         :sp:

*** Egregious screenshots

[[file:../static/cadr-zmacs.jpg]]

** An intermission: women in lisp history                                :sp:

** "Vim is an emacs", and Blender is too?

Sorta... retrofitted awkwardly!


