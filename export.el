(require 'org)

(defun export-reveal-talk (file-path)
  (save-excursion
    (require 'ox-reveal)
    (find-file file-path)
    (org-mode)
    (org-reveal-export-to-html)))

(defun export-beamer-talk (file-path)
  (save-excursion
    (require 'ox-beamer)
    (find-file file-path)
    (org-mode)
    (org-beamer-export-to-pdf)))


(defun export-html-talk (file-path)
  (save-excursion
    (find-file file-path)
    (org-mode)
    (org-html-export-to-html)))

