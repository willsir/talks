;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

(use-modules (guix packages)
             (guix licenses)
             (guix git-download)
             (guix build-system gnu)
             (gnu packages)
             (gnu packages emacs)
             (gnu packages base)
             (gnu packages texlive))

(package
  (name "talks-env")
  (version "git")
  (source #f)
  (build-system gnu-build-system)
  (synopsis #f)
  (description #f)
  (license #f)
  (home-page #f)
  (inputs
   `(("texlive" ,texlive)
     ("emacs" ,emacs)))
  (propagated-inputs
   `(("coreutils" ,coreutils))))  ; convenience :)
