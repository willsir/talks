;; Set up el-get directories
(setq el-get-dir (expand-file-name "./.el-get/"))
(unless (file-exists-p el-get-dir)
  (make-directory el-get-dir))

(add-to-list 'load-path (expand-file-name "./.el-get/el-get"))

;; Install el-get if necessary

(unless (require 'el-get nil t)
  (message "No el-get found... installing!")
  (load-file "el-get-install.el")
  (message "done"))

;; Load el-get
(require 'el-get)

;; local definition of sources
(setq el-get-sources
      '((:name "org-reveal"
         :description "reveal.js stuff for orgmode"
         :type git
         :url "https://github.com/yjwen/org-reveal.git")
        (:name "reveal.js"
         :type git
         :url "https://github.com/hakimel/reveal.js.git")))

(setq my-packages
      (append
       '("org-mode" "htmlize")
       (mapcar 'el-get-source-name el-get-sources)))

;; Install packages!
(el-get 'sync my-packages)

