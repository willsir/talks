* Intro

I couldn't be more honored to be up here speaking and to be the best man
at my brother's wedding.
Nor could I be more pleased to see my brother marrying a person as
wonderful as Jill.

I'm also pleased that they chose the topic of their wedding to be
about games.
I think this is super appropriate, and not just because Steve and Jill
clearly love games (clearly they do)
For me, it is especially fitting because games are largely how I
have come to be so close to the lives of Steve and Jill.

This will be an unusual wedding speech, because I'm going to try to
convince you that through games, we can find meaningful lessons about
life...
Meaningful enough, hopefully, for a wedding speech.
   
So what are the core elements of a game?
That's a big topic!  But for here, we'll restrict to three aspects:

 - Rules: Which give us constraints, but also structure.
 - Chaos: Randomness, which forces us to adapt to the unexpected.
   (Hold up dice!)
 - Immersion: The feeling that you are taking part in the world
   of the game, from its story to artwork to maybe even music.
   
With that framework in mind...

* Childhood

Steve and I spent most of our childhood very close, and from the
beginnings, we played the kind of games many children do, those of
make-believe.
We would build settings and characters out of legos, sing songs, and
tell stories.

It was pure immersion, and barely even bounded by the rules of
narrative convention.  The only chaos we had was the chaos of being
children, but there were no boundaries.  I was more or less a game
master without a rulebook or dice, in retrospect.

In this way, in our youngest days, Steve and I grew very close.  But
our games lacked boundaries, and any good game player knows that risks
bring rewards.

Unsurprisingly as we grew older, we became more drawn to video games.
As the older brother I was frequently the distributor of these,
sometimes even bringing a bounty of illicit ROMs.

Hey kid, check out this Japanese fighting game, it uses an exclusively
cinematic fighting mechanic.
Hey kid, you want a copy of Chrono Trigger?  Best game ever made, you're
gonna have the time of your life!

These games brought the boundaries and chaos our games of youngest
days lacked, at a cost to immersion: while the graphics were beautiful,
the sounds majestic, the story was always constrained.

Even the games with most narrative freedom were at best like a
choose your own adventure book on steroroids.

This desire to build a world that could bend to *your* constraints and
*your* storyline I think probably drew us both to programming, and
here I was also pleased to be the perveyor of materials, except this time
I didn't know what I was doing and I think we both got stuck.

Unfortunately at this time I think Steve and I both drifted apart
quite a bit.  I didn't even realize how much, until we had the
opportunity to connect again.

* Adulthood

Flash forward over a decade.

I'm married, Morgan and I are moving to Madison, I'm a programmer
(unsurprisingly), Steve is graduating as a programmer (unsurprisingly)
and starting his career.

A few months in I get a text message from Steve.
"Do you know any vegetarian restaurants?"
I text back "Are you dating a vegetarian?"
Steve texts back, "Would you like to meet her?"

Morgan and I liked Jill as soon as we met her, but I think we really
came to know and love Jill through playing games, specifically,
tabletop role playing games.

If you've never played a role playing game, maybe you have some
misconceptions.  Role playing games are really "collaborative
storytelling with dice".  You have some rules, you have chaos through
dice, but you're really there for the immersion.

You can learn a lot about a person by watching them get involved in a
character, and I quickly learned how witty, smart, and caring Jill
was.  I also learned how much she loved animals; playing a character
who was a magical hobo with a dog, she turned to Steve and said,
"If anything happens to my dog steve, *you're* in trouble."

Steve and Jill also introduced me to a world of board games I never
realized I was interested in.  I thought I wasn't interested in board
games; too much time studying the rules, too many fiddly pieces.  I
was wrong.

Up until this point, without realizing it, I always saw myself as the
"big brother" in our relationship; *I* was the one mediating the
make-believe, *I* was the one dropping off SNES roms and introducing
video game genres, *I* was the one who was the first game master at
the role playing game table.

I was always interested in the immersion of a system, and I enjoyed
chaos as a way to shake things up; rules were only a means to an end
to get there.  The last five years have changed my world view, and
even my understanding of my brother; if you know him, you know how
much he loves to explore rules and the ways things tick.  Steve's love
of rules became infectious, and even bled over into our careers; I
began to see my love of programming changed through this exploration
of rules, as we would meet up for nerd-out night, and Steve would
encourage me to look deeper and see the beauty of a system for what it
was.  I've been humbled and honored by this.

I also began to see inner depths and struggles of my brother I never
knew was there.  It's easy to miss with Steve's externally quiet and
cool facade that there's a turmoil, a storm he reckons with, a storm
of the soul.  I suppose each of us has a unique storm within us, but
somehow I had never seen it or even thought it to be there.

Through playing games together, I began how Steve and Jill
counterbalance each other.  There's a delightful meta-language between
Steve and Jill (especially at the gaming table) which is a kind of
playful antagonism.  But if you look closely, you also see it's
secretly a language of cooperation.  So alike and yet so different, I
have seen too how they anchor each other within the storm of their
souls.

Jill, that you could bring this gift to my brother touches me deeply.

* Your life

Much of this speech has reflected upon the past.  I would like to
speak now to the future while returning to the theme.

 - Rules: Wedding speeches frequently give out advice, and what is
   advice but a prescribed set of rules?  But I'd say there are really
   only two rules or advice I give to any relationship:

    1) Always be open in communication with each other, and use this
       to establish, cooperatively, the rules of your life.
    2) Aside from rule number 1, all other relationship and advice
       from others can be discarded, including but not limited to
       rule number two.

   Playtesting at this phase can sometimes be fraught, but also a lot
   of fun.  Listen to each other, and you'll strike the right balance.

 - Chaos: It's nice to have stability, but life throws things at you,
   and sometimes you have to adjust to what comes at you.  Again,
   follow rule number one with open communication, and you'll be okay.
   
 - Immersion: /transition to a toast/
   Now you two have your life ahead of you.  Live it,
   experience it, and love it together.  I know you will.

Now have fun playing.
