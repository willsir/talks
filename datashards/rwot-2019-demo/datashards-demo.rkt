#lang slideshow

(require racket/draw
         slideshow/text
         pict/code
         (prefix-in cute: 2htdp/planetcute))

;; Set the font
(current-main-font (cons (make-object color% 42 40 40)
                         "Inconsolata"))

#;(current-main-font (cons (make-object color% 237 237 237)
                         "Inconsolata"))

#;(set-page-numbers-visible! #f)

(define default-slide-assembler
  (current-slide-assembler))

;; Set a light cream background
(current-slide-assembler
 (lambda (s v-sep c)
   (define width
     (+ (* margin 2) client-w))
   (define height
     (+ (* margin 2) client-h))
   (ct-superimpose
    (inset (filled-rectangle width height
                             #:color (make-object color%
                                                  ;; 50 47 47
                                                  237 237 237)
                             #:draw-border? #f)
           (- margin))
    (default-slide-assembler s v-sep c))))

(define (scaled-image filename [height-adjust 0])
  (scale-to-fit (bitmap filename)
                client-w (- client-h height-adjust)))

(define (inset-t text [frame? #f])
  (if frame?
      (frame (inset (t text) 1)
             #:color "DarkGreen"
             #:line-width 3)
      (inset (t text) 1)))

(slide
 (scaled-image "../static/Datashards-logo.png" 350)
 (small (italic (t "Demo edition!")))

 (small (small (blank-line)))
 
 (small (bold (t "Serge Wroclawski")))
 (small (bold (t "Christopher Lemmer Webber"))))

(slide
 (scaled-image "../static/Datashards-logo.png" 500)
 'alts
 (list
  (list
   (hc-append
    (inset-t "Secure")
    (t ", ")
    (inset-t "distributed storage")
    (t " ")
    (inset-t "primitives for the web")))
  (list
   (hc-append
    (inset-t "Secure" #t)
    (t ", ")
    (inset-t "distributed storage")
    (t " ")
    (inset-t "primitives for the web"))
   (item "Only intended recipients can see messages")
   (item "Safer for node operators to help network")
   (item "Secure against size-of-file-attacks"))
  (list
   (hc-append
    (inset-t "Secure")
    (t ", ")
    (inset-t "distributed storage" #t)
    (t " ")
    (inset-t "primitives for the web"))
   (item "Many different \"stores\"")
   (subitem "A \"Secure Data Hub\" web store!")
   (subitem "Your local filesystem!")
   (subitem "USB keys!")
   (subitem "Distributed over a gossip network/DHT!")
   (item "Nodes can host content, but don't know what it is"))
  (list
   (hc-append
    (inset-t "Secure")
    (t ", ")
    (inset-t "distributed storage")
    (t " ")
    (inset-t "primitives for the web" #t))
   (item "Meant to be foundational, the way http(s) is foundational for"
         "live content")
   (item "Two new URI schemas:")
   (subitem "idsc: Immutable, unchanging data")
   (subitem "mdsc: Mutable, updateable data")
   (item "Data can have the same name, but can live in many places"))))

(slide
 (t "Racket AND Python implementations?  Already???")
 (t "Demo time!"))
